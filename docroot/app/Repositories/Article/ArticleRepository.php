<?php
/**
 * Created by PhpStorm.
 * User: salt
 * Date: 04/02/19
 * Time: 12:14
 */

namespace App\Repositories\Article;

use App\Models\Article;
use App\Models\ArticleRevision;
use App\Models\ArticleTranslation;
use App\Models\Bookmarks;
use App\Repositories\Article\ArticleTranslationRepository as Translation;
use App\Repositories\Article\ArticleRevisionRepository as Revision;
use App\Repositories\RepositoryInterface;
use Illuminate\Database\Eloquent\Model;
use App\Models\ArticleNotes;

class ArticleRepository implements RepositoryInterface
{
    public function all(array $columns = ['*'])
    {
        $status = app('request')->get('status');
        $highlight = app('request')->get('highlight');
        $limit = app('request')->get('limit');
        $page = app('request')->get('page');
        $locale = app('request')->input('locale');

        $articleTrans = ArticleTranslation::where('locale', $locale)->get();
        $articleid = array_map(function ($object) { return $object->articleId; }, json_decode($articleTrans));

        $articleBuilder = Article::filterStatus($status)->filterHighlight($highlight)->whereIn('id', $articleid);

        if (is_null($page)) {
            return $articleBuilder->get();
        }

        return $articleBuilder->paginate(
            $limit ?? 8,
            ['*'],
            'page',
            $page ?? 1
        );
    }

    public function count() : int
    {
        return Article::count();
    }

    public function paginate(int $perPage = 15, $columns = ['*'])
    {
        return null;
    }

    public function create(array $data): Model
    {
        $article = Article::create($data);
        $articleData['articleId'] = $article->id;
        $data = array_merge($data, $articleData);

        $translation = new Translation();
        $translation = $translation->create($data);

        $articleSlug['slug'] = $translation->slug;
        $data = array_merge($data, $articleSlug);
        $articleMetadata['metadata'] = json_encode($data);
        $data = array_merge($data, $articleMetadata);

        $getDataVersion = ArticleRevision::where(["articleId" => $article->id, 'locale' => $data['locale']])->first();

        if($getDataVersion){
            $articleVersion['version'] = $getDataVersion->version + 1;
        }else{
            $articleVersion['version'] = 0;
        }

        $data = array_merge($data, $articleVersion);

        $revision = new Revision();
        $revision->create($data);

        return $article;
    }

    public function update(array $data, int $id)
    {
        $article = $this->find($id);
        $article->update($data);

        $articleData['articleId'] = $id;
        $data = array_merge($data, $articleData);

        $getDataTranslation = ArticleTranslation::where(["articleId" => $article->id, 'locale' => $data['locale']])->first();
        $translation = new Translation();

        if($getDataTranslation){
            $translation = $translation->update($data, $getDataTranslation->id);
        }else{
            $translation = $translation->create($data);
        }

        $articleSlug['slug'] = $translation->slug;
        $data = array_merge($data, $articleSlug);

        $articleMetadata['metadata'] = json_encode($data);
        $data = array_merge($data, $articleMetadata);

        $getDataVersion = ArticleRevision::where(["articleId" => $article->id, 'locale' => $data['locale']])->first();

        if($getDataVersion){
            $articleVersion['version'] = $getDataVersion->version + 1;
        }else{
            $articleVersion['version'] = 0;
        }

        $data = array_merge($data, $articleVersion);
        $data['createdBy'] = $data['userId'];
        $revision = new Revision();
        $revision->create($data);

        return $article->refresh();
    }

    public function updateBy(string $field, string $value, array $data)
    {
        $getIdFromSlug = new Translation();
        $getIdFromSlug = $getIdFromSlug->findBy('slug', $value);

        if(!$getIdFromSlug)
        {
            throw new \App\Exceptions\ModelNotFoundException;
        }

        $article = $this->find($getIdFromSlug->articleId);
        $article->update($data);

        $getDataTranslation = ArticleTranslation::where(["articleId" => $getIdFromSlug->articleId, 'locale' => $data['locale']])->first();

        $translation = new Translation();

        if($getDataTranslation){
            $translation = $translation->update($data, $getDataTranslation->id);
        }else{
            $translation = $translation->create($data);
        }

        $articleSlug['slug'] = $translation->slug;
        $data = array_merge($data, $articleSlug);

        $articleMetadata['metadata'] = json_encode($data);
        $data = array_merge($data, $articleMetadata);

        $getDataVersion = ArticleRevision::where("articleId", $article->id)->first();

        if($getDataVersion){
            $articleVersion['version'] = $getDataVersion->version + 1;
        }else{
            $articleVersion['version'] = 0;
        }

        $data = array_merge($data, $articleVersion);
        $data['createdBy'] = $data['userId'];
        $revision = new Revision();
        $revision->create($data);

        return $article->refresh();
    }

    public function delete(int $id)
    {
        $article = $this->find($id);
        $article->delete();
        $video_detail = ArticleTranslation::Query()->where('articleId',$id)->delete();
    }

    public function find(int $id, $columns = array('*'))
    {
        try {
            $article = Article::findOrFail($id);
        }
        catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            throw new \App\Exceptions\ModelNotFoundException;
        }

        return $article;
    }

    public function findBy(string $field, string $value, $columns = ['*'])
    {
        $status = app('request')->get('status');
        try {
            $article = Article::filterStatus($status)->where($field, $value)->get();
        }
        catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            throw new \App\Exceptions\ModelNotFoundException;
        }

        return $article;
    }

    public function getBySlug($locale, $slug)
    {
        try {
            $article = ArticleTranslation::where(['locale' => $locale, 'slug' => $slug])->first();
        }
        catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            throw new \App\Exceptions\ModelNotFoundException;
        }
        if(!isset($article->articleId)){
            return null;
        }
        $articleId = $article->articleId;

        $article = $this->find($articleId);

        return $article;
    }

    public function getRelated($locale, $slug)
    {
        try {
            $article = ArticleTranslation::where(['locale' => $locale, 'slug' => $slug])->first();
        }
        catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            throw new \App\Exceptions\ModelNotFoundException;
        }

        $articleId = $article->articleId;

        $article = $this->find($articleId);
        $tags = $article->tags;

        if(!$tags)
        {
            throw new \App\Exceptions\ModelNotFoundException;
        }

        $tags = substr($tags, 1);
        $tags = substr($tags, 0, -1);
        $tags = explode(",", $tags);

        $limit = app('request')->get('limit');
        $page = app('request')->get('page');
        $status = app('request')->get('status');
        $highlight = app('request')->get('highlight');


        $articleTrans = ArticleTranslation::where('locale', $locale)->get();
        $articleid = array_map(function ($object) { return $object->articleId; }, json_decode($articleTrans));


        $articleBuilder = Article::filterStatus($status)->filterHighlight($highlight)
            ->where('id', '!=', $articleId)
            ->whereIn('id', $articleid)
            ->when($tags, function ($articleBuilder, $tags) {
            for ($i = 0; $i < count($tags); $i++) {
                $articleBuilder->where('tags', 'like', '%'.$tags[$i].'%');
            }
        });

        if (is_null($page)) {
            return $articleBuilder->get();
        }

        return $articleBuilder->paginate(
            $limit ?? 8,
            ['*'],
            'page',
            $page ?? 1
        );
    }
    public function publish($data,$id){
        $article = $this->find($id);
        $article->update($data);

        return $article->refresh();
    }
    public function addRevision(array $data,$id){
        $data['articleId'] = $id;
        $notes = ArticleNotes::create($data);
        if($notes){
            $article = $this->find($id);
            $article->update([
                'status' => 3
            ]);
        }
        return $article->refresh();
    }
    public function editRevision(array $data,$id){
        $notes = ArticleNotes::find($id);
        $notes->update([
            'content' => $data['content'],
            'notes' => $data['notes']
        ]);
        $article = $notes->refresh();
        $article = $this->find($article->articleId);
        return $article;
    }
    public function solveRevision($id){
        $solve = app('request')->get('solve');
        $notes = ArticleNotes::find($id);
        if($solve == 1){
            $notes->update([
                'status' => 1,
            ]);
            $article = $notes->refresh();
            $article = $this->find($article->articleId);
        }elseif($solve == 0){
            $notes->update([
                'status' => 0,
            ]);
            $article = $notes->refresh();
            $article = $this->find($article->articleId);
            $article->update([
                'status' => 3
            ]);
        }

        return $article->refresh();
    }
    public function bookmarks($data, $slug,$type,$deleted){
        $article = $this->getBySlug($data->get('locale'),$slug);
        if(empty($article)){
            return null;
        }
        try {
            $bookmarks = Bookmarks::where('articleId',$article->id)
                ->where('type',1)
                ->where('userId',$data['userId'])
                ->firstOrFail();
        }
        catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            Bookmarks::create([
                'userId' => $data['userId'],
                'articleId' => $article->id,
                'type' => $type,
            ]);
            return $article;
        }
        Bookmarks::where('id',$bookmarks->id)->update([
            'isDeleted' => $deleted,
        ]);
        return $article;
    }
}