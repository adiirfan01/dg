<?php
/**
 * Created by PhpStorm.
 * User: salt
 * Date: 04/02/19
 * Time: 12:15
 */

namespace App\Repositories\Video;

use App\Models\VideoComment;
use App\Models\VideoLikeComment;
use App\Models\Video;
use App\Repositories\RepositoryInterface;
use Illuminate\Database\Eloquent\Model;

class VideoCommentRepository implements RepositoryInterface
{
    public function all(array $columns = ['*'])
    {
        $video = app('request')->get('video');
        $limit = app('request')->get('limit');
        $page = app('request')->get('page');
        $commentBuilder = Comment::Videos($video);

        if(empty($video)){
            return null;
        }
        if (is_null($page)) {
            return $commentBuilder->get();
        }

        return $commentBuilder->paginate(
            $limit ?? 5,
            ['*'],
            'page',
            $page ?? 1
        );
    }

    public function count() : int
    {
        return VideoComment::count();
    }

    public function paginate(int $perPage = 15, $columns = ['*'])
    {
        return null;
    }

    public function create(array $data): Model
    {
        $check = Video::find($data['videoId']);
        if ($check){
            $comment = VideoComment::create($data);
            return $comment;
        }else{
            throw new \App\Exceptions\ModelNotFoundException;
        }
    }

    public function update(array $data, int $id)
    {
        $comment = $this->find($id);

        $comment->update($data);

        return $comment->refresh();
    }

    public function updateBy(string $field, string $value, array $data)
    {
        $comment = $this->findBy($field, $value);
        $comment->update($data);

        return $comment->refresh();
    }

    public function delete(int $id)
    {
        return null;
    }

    public function find(int $id, $columns = array('*'))
    {
        try {
            $comment = VideoComment::findOrFail($id);
        }
        catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            throw new \App\Exceptions\ModelNotFoundException;
        }

        return $comment;
    }

    public function findBy(string $field, string $value, $columns = ['*'])
    {
        try {
            $comment = VideoComment::where($field, $value)->get();
        }
        catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            throw new \App\Exceptions\ModelNotFoundException;
        }

        return $comment;
    }
    public function like(array $data,$id,$type){
        $checkcomment = VideoComment::where('id',$id)->first();
        if(empty($checkcomment)){
            return null;
        }
        try {
            $comment = VideoLikeComment::where('commentId', $id)
                ->where('userId',$data['userId'])
                ->firstOrFail();
        }
        catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            $comment = VideoLikeComment::create([
                'userId' => $data['userId'],
                'commentId' => $id,
                'type' => $type,
                'isVideo' => 0
            ]);
            return $comment;
        }
        VideoLikeComment::where('id',$comment->id)->update([
            'type' => $type,
            'isDeleted' => 0,
        ]);
        return $comment;
    }
    public function unlike(array $data,$id){
        $checkcomment = VideoComment::where('id',$id)->first();
        if(empty($checkcomment)){
            return null;
        }
        try {
            $comment = VideoLikeComment::where('commentId', $id)
                ->where('userId',$data['userId'])
                ->firstOrFail();
        }
        catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return null;
        }
        VideoLikeComment::where('id',$comment->id)->update([
            'isDeleted' => 1,
        ]);
        return $comment;
    }
}