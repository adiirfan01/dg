<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnalyticTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vasAnalytic', function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->string('uuid', 255);
            $table->bigInteger('vasId');
            $table->bigInteger('categoryId');
            $table->enum('type', ['share', 'view'])->default('view');
            $table->string('count');
            $table->string('locale');
            $table->dateTime('createdDate');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vasAnalytic');
    }
}
