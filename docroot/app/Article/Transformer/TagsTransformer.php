<?php
/**
 * Created by PhpStorm.
 * User: salt
 * Date: 04/02/19
 * Time: 13:44
 */

namespace App\Article\Transformer;

use App\Models\Tags;
use League\Fractal\TransformerAbstract;

class TagsTransformer extends TransformerAbstract
{
    protected $locale;
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $defaultIncludes = [];

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(Tags $tags)
    {
        return [
            'id' => $tags->id,
            'slug' => $tags->slug
        ];
    }
}