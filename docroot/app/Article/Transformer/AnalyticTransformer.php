<?php
/**
 * Created by PhpStorm.
 * User: salt
 * Date: 04/02/19
 * Time: 13:20
 */

namespace App\Article\Transformer;

use App\Models\Analytic;
use App\Article\Transformer\ArticleTransformer;
use App\Article\Transformer\VideoTransformer;
use App\Models\Category;
use League\Fractal\TransformerAbstract;
use App\Models\Article;
use App\Models\Video;
use App\Helpers\GetDataUser;
use App\Models\Bookmarks;

class AnalyticTransformer extends TransformerAbstract
{
    protected $locale;

    /**
     * List of resources default to include
     *
     * @var array
     */
    protected $defaultIncludes = [
    ];

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(Analytic $analytic)
    {
        if($analytic->typeVas == 'video'){
            $data = $this->getVideo($analytic->vasId);
            $detail = $this->detailVideo($data);
        }else{
            $data = $this->getArticle($analytic->vasId);
            $detail = $this->detailArticle($data);
        }
        return [
            'id' => $data->id,
            'author' => $this->getDataAuth0($data->authorId),
            'publishedDate' => $data->publishedDate,
            'category' => $this->category($data->categoryId),
            'slug' => $detail['slug'],
            'title' => $detail['title'],
            'image' =>  (isset($data->image)) ? $data->image : null,
            'videoUrl' =>  (isset($data->videoUrl)) ? $this->getVideoId($data->videoUrl) : null,
            'type' => $data->type,
            'highlight' => $data->highlight,
            'isBookmarked' => $this->checkBookmark($data->id,$data->type)
        ];
    }
    public function checkBookmark($id,$type){
        if($type == 'article'){
            $type = 1;
        }else{
            $type = 2;
        }
        $userId = app('request')->input('userId');
        if(is_null($userId)){
            return 0;
        }
        $bookmarks = Bookmarks::where('articleId',$id)
            ->where('type',$type)
            ->where('userId',$userId)
            ->where('isDeleted',0)
            ->first();
        if(empty($bookmarks)){
            return 0;
        }
        return 1;
    }
    public function getVideo($id){
        $video = Video::where('id',$id)
            ->with('detail')
            ->first();
        $video->type = 'video';
        return $video;
    }
    public function getArticle($id){
        $article = Article::where('id',$id)
            ->with('translation')
            ->first();
        $article->type = 'article';
        return $article;
    }
    public function detailArticle($data){
        $locale = app('request')->input('locale');
        $data = $data->translation->filter(function ($data) use($locale) {
            return $data->locale == $locale;
        })->first();
        return $data;
    }
    public function detailVideo($data){
        $locale = app('request')->input('locale');
        $data = $data->detail->filter(function ($data) use($locale) {
            return $data->locale == $locale;
        })->first();
        return $data;
    }
    public function getDataAuth0($userId)
    {
        $dataUser = new GetDataUser();
        return $dataUser->getMinimalProfile($userId);
    }
    public function category($categoryId)
    {
        $locale = app('request')->input('locale');
        $data = Category::query()->where('id',$categoryId)->with('translation')->first();
        $data = $data->translation->filter(function ($data) use($locale) {
            return $data->locale == $locale;
        })->first();
        return $data;
    }
    public function getVideoId($data){
        $data = str_replace('https://youtu.be/','',$data);
        return $data;
    }


}