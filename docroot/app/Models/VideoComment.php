<?php

namespace App\Models;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;

/**
 * @property int $id
 * @property int $articleId
 * @property int $userId
 * @property string $comment
 * @property \Carbon\Carbon $createdDate
 * @property string $createdBy
 * @property \Carbon\Carbon $updatedDate
 * @property string $updatedBy
 * @property boolean $isDeleted
 */
class VideoComment extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'video_comment';

    const CREATED_AT = 'createdDate';
    const UPDATED_AT = 'updatedDate';

    /**
     * @var array
     */
    protected $fillable = [
        'videoId',
        'userId',
        'comment',
        'like',
        'dislike',
        'isDeleted',
        'createdDate',
        'updatedDate',
        'createdBy',
        'updatedBy'
    ];

    /**
     * Get the Video
     */
    public function video()
    {
        return $this->belongsTo(Video::class, 'videoId');
    }
    /**
     * Get the Reply Comment
     */
    public function replyComment()
    {
        return $this->hasMany(VideoCommentReply::class, 'commentId','id');
    }

    public function scopeVideos($query, $videoId)
    {
        if (is_null($videoId)) return $query;

        return $query->where('videoId', '=', $videoId);
    }

}
