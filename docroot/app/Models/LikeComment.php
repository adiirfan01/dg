<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;

class LikeComment extends Model
{
    protected $table = 'comment_like';
    protected $fillable = [
        'commentId',
        'userId',
        'type',
        'isDeleted'
    ];
}
