<?php
/**
 * Created by PhpStorm.
 * User: salt
 * Date: 04/02/19
 * Time: 13:50
 */

namespace App\Http\Controllers\V1\Tags;

use App\Http\Request\StoreEditTagsRequest;
use App\Http\Request\StoreTagsRequest;
use Illuminate\Http\Request;

class TagsController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $category = $this->repo->all();

        return $this->getSuccess('success', $category);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save(StoreTagsRequest $request)
    {
        $category = $this->repo->create($request->all());

        return $this->getSuccess('success', $category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug, StoreEditTagsRequest $request)
    {
        $request->merge([
            'slug'=> $slug
        ]);

        $category = $this->repo->updateBy('slug', $slug, $request->all());

        return $this->getSuccess('success', $category);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($slug)
    {
        $this->repo->delete($slug);

        return $this->getSuccess('deleted');
    }
}