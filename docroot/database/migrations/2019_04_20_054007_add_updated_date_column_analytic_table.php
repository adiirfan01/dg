<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUpdatedDateColumnAnalyticTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vasAnalytic', function (Blueprint $table) {
            //
            $table->dateTime('updatedDate')->nullable(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vasAnalytic', function (Blueprint $table) {
            //
            $table->dropColumn('updatedDate');
        });
    }
}
