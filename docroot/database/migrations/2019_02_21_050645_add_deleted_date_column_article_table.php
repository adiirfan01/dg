<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDeletedDateColumnArticleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('article', function (Blueprint $table) {
            $table->datetime('deletedDate')->nullable();
        });

        Schema::table('article_notes', function (Blueprint $table) {
            $table->datetime('deletedDate')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('article', function (Blueprint $table) {
            $table->datetime('deletedDate')->nullable();
        });

        Schema::table('article_notes', function (Blueprint $table) {
            $table->datetime('deletedDate')->nullable();
        });
    }
}
