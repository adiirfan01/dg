<?php
/**
 * Created by PhpStorm.
 * User: maleakhi
 * Date: 3/19/19
 * Time: 6:41 PM
 */

namespace App\Helpers;

use App\Helpers\Connection as Connect;
use Illuminate\Http\JsonResponse;

class GetDataUser
{
    public function getMinimalProfile($userId)
    {
        $data = [];
        $url = "/api/v1/profile/minimal-profile";
        $connect = new Connect();
        try {
            $getDataUser = $connect->getRequest(getenv('URL_USER').$url, $userId, "get-profilepicture");
        } catch (\Exception $e) {
            $data['fullName'] = "";
            $data['profilePicture'] = "";
            return $data;
        }

        if($getDataUser instanceof JsonResponse){
            $getDataUser = json_decode($getDataUser->content());
        }

        if($getDataUser->status->code == 0){
            if($getDataUser->data){
                $data['fullName'] = $getDataUser->data->fullName;
                $data['profilePicture'] = $getDataUser->data->profilePicture;
                $data['userName'] = $getDataUser->data->userName;
            }else{
                $data['fullName'] = "";
                $data['profilePicture'] = "";
            }
        }else{
            $data['fullName'] = "";
            $data['profilePicture'] = "";
        }

        return $data;
    }

    public function getUserProfile($userId)
    {
        $data = [];
        $url = "/api/v1/profile/id-profile";
        $connect = new Connect();
        try {
            $getDataUser = $connect->getRequest(getenv('URL_USER').$url, $userId, "get-phone-email");
        } catch (\Exception $e) {
            $data['phoneNumber'] = "";
            $data['email'] = "";
            return $data;
        }

        if($getDataUser instanceof JsonResponse){
            $getDataUser = json_decode($getDataUser->content());
        }

        if($getDataUser->status->code == 0){
            if($getDataUser->data){
                $data['phoneNumber'] = $getDataUser->data->phoneNumber;
                $data['email'] = $getDataUser->data->email;
            }else{
                $data['phoneNumber'] = "";
                $data['email'] = "";
            }
        }else{
            $data['phoneNumber'] = "";
            $data['email'] = "";
        }

        return $data;
    }
}