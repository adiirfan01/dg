<?php
/**
 * Created by PhpStorm.
 * User: salt
 * Date: 04/02/19
 * Time: 13:49
 */

namespace App\Http\Controllers\V1\Article;

use App\Http\Controllers\V1\Article;
use App\Http\Request\StoreArticleRequest;
use App\Http\Request\StoreUpdateArticleRequest;
use Illuminate\Http\Request;
use App\Http\Request\StorePublishRequest;
use App\Http\Request\StoreDraftRequest;

class ArticleController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $article = $this->repo->all();

        return $this->getSuccess('success', $article);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save(StoreArticleRequest $request)
    {
        $request->merge([
            'status' => 2,
        ]);
        $article = $this->repo->create($request->all());

        return $this->getSuccess('success', $article);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, StoreUpdateArticleRequest $request)
    {
        $request->merge([
            'publishedDate' => null,
            'status' => 2,
        ]);
        $article = $this->repo->update($request->all(), $id);

        return $this->getSuccess('success', $article);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $this->repo->delete($id);

        return $this->getSuccess('Deleted');
    }
    public function publish(int $id,StorePublishRequest $request)
    {
        $article = $this->repo->find($id);
        $request->merge([
            'publishedDate' => date('y-m-d h:i:s'),
            'status' => 1,
        ]);

        $article = $this->repo->publish($request->all(), $id);
        return $this->getSuccess('success', $article);
    }
    public function addHighlight(int $id,StorePublishRequest $request)
    {
        $article = $this->repo->find($id);
        $request->merge([
            'sticky' => 1,
        ]);

        $article = $this->repo->publish($request->all(), $id);
        return $this->getSuccess('success', $article);
    }
    public function removeHighlight(int $id,StorePublishRequest $request)
    {
        $article = $this->repo->find($id);
        $request->merge([
            'sticky' => 0,
        ]);

        $article = $this->repo->publish($request->all(), $id);
        return $this->getSuccess('success', $article);
    }

    public function rejected(int $id,StorePublishRequest $request)
    {
        $article = $this->repo->find($id);
        $request->merge([
            'status' => 4,
        ]);
        $article = $this->repo->publish($request->all(), $id);
        return $this->getSuccess('success', $article);
    }
    public function saveToDraft(StoreDraftRequest $request)
    {
        $article = $this->repo->create($request->all());
        return $this->getSuccess('success', $article);
    }
    public function editDraft($id, StoreDraftRequest $request)
    {
        $request->merge([
            'status' => 0
        ]);
        $article = $this->repo->update($request->all(), $id);
        return $this->getSuccess('success', $article);
    }
    public function submit(string $slug,Request $request)
    {
        $article = $this->repo->getBySlug($request->get('locale'), $slug);
        if(!isset($article->status)){
            return $this->getError('Article Tidak Ada');
        }
        if(!$article->status == 0){
            return $this->getError('Submit gagal');
        }
        $request->merge([
            'status' => 2,
        ]);

        $article = $this->repo->publish($request->all(), $article->id);
        return $this->getSuccess('success', $article);
    }
    public function bookmarks($slug, Request $request)
    {
        $article = $this->repo->bookmarks($request,$slug,1,0);
        return $this->getSuccess('success', $article);
    }
    public function unbookmarks($slug, Request $request)
    {
        $article = $this->repo->bookmarks($request,$slug,1,1);
        return $this->getSuccess('success', $article);
    }
}