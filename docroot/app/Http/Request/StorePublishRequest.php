<?php

namespace App\Http\Request;

use Illuminate\Validation\Rule;

class StorePublishRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'userId' => 'required',
        ];
    }


    protected function getValidatorInstance()
    {
        $this->getInputSource()->replace($this->modifyData());
        $validator = parent::getValidatorInstance();

        return $validator;
    }

    protected function modifyData()
    {
        $data = $this->validationData();
        if($data['userId'] == null){
            $data['createdBy'] = "sms|5c6e37069e6bbdc8bc73fa9c";
        }else{
            $data['createdBy'] = $data['userId'];
        }
        return $data;
    }
}
