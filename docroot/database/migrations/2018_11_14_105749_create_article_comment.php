<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticleComment extends Migration
{
    private const ARTICLE_COMMENT_TABLE = "article_comment";
    private const ARTICLE_REPLY_COMMENT_TABLE = "article_reply_comment";
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::create(self::ARTICLE_COMMENT_TABLE, function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->bigInteger('articleId');
            $table->bigInteger('userId');
            $table->text('comment');
            $table->dateTime('createdDate');
            $table->string('createdBy', 255);
            $table->dateTime('updatedDate')->nullable(true);
            $table->string('updatedBy', 255)->nullable(true);
            $table->tinyInteger('isDeleted')->default(0);
        });

        Schema::create(self::ARTICLE_REPLY_COMMENT_TABLE, function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->bigInteger('commentId');
            $table->bigInteger('userId');
            $table->text('comment');
            $table->dateTime('createdDate');
            $table->string('createdBy', 255);
            $table->dateTime('updatedDate')->nullable(true);
            $table->string('updatedBy', 255)->nullable(true);
            $table->tinyInteger('isDeleted')->default(0);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(self::ARTICLE_COMMENT_TABLE);
        Schema::dropIfExists(self::ARTICLE_REPLY_COMMENT_TABLE);
    }
}
