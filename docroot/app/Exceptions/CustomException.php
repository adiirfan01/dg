<?php
/**
 * Created by PhpStorm.
 * User: salt
 * Date: 01/02/19
 * Time: 11:22
 */

namespace App\Exceptions;


class CustomException extends \Exception
{
    private $params;

    public function setParams(array $params)
    {
        $this->params = $params;
    }

    public function getParams()
    {
        return $this->params;
    }
}