<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bookmarks extends Model
{
    protected $table = 'article_bookmarks';
    protected $fillable = [
        'articleId',
        'userId',
        'type',
        'isDeleted'
    ];
}
