<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;

/**
 * @property int $id
 * @property int $articleId
 * @property string $title
 * @property string $slug
 * @property string $excerpt
 * @property string $content
 * @property string $locale
 * @property string $metaTitle
 * @property string $metaDescription
 * @property string $metaKeyword
 * @property string $metaFbTitle
 * @property string $metaFbDescription
 * @property string $metaFbImage
 * @property string $metaTwitterTitle
 * @property string $metaTwitterDescription
 * @property string $metaTwitterImage
 * @property string $metaRobotsIndex
 * @property string $metaRobotsAdvance
 * @property string $metaRobotsFollow
 * @property string $metaCanonicalUrl
 */

class ArticleTranslation extends Model
{
    use Sluggable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'article_translation';

    /**
     * @var array
     */
    protected $fillable = [
        'articleId',
        'title',
        'slug',
        'locale',
        'excerpt',
        'content',
        'metaTitle',
        'metaDescription',
        'metaKeyword',
        'metaFbTitle',
        'metaFbDescription',
        'metaFbImage',
        'metaTwitterTitle',
        'metaTwitterDescription',
        'metaTwitterImage',
        'metaRobotsIndex',
        'metaRobotsAdvance',
        'metaRobotsFollow',
        'metaCanonicalUrl'
    ];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Get the Article
     */
    public function article()
    {
        return $this->belongsTo(Article::class, 'articleId');
    }
    public function scopeFilterSearch($query, $search)
    {
        if (is_null($search)) return $query;
        $query->Where('title', 'LIKE', '%'.$search.'%');
        return $query;
    }

}
