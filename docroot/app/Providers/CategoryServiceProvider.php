<?php
/**
 * Created by PhpStorm.
 * User: salt
 * Date: 31/01/19
 * Time: 16:31
 */
namespace App\Providers;

use App\Http\Controllers\V1\Category\{CategoryController, BaseController as BaseCategory};
use Illuminate\Support\ServiceProvider;
use App\Repositories\RepositoryInterface;
use App\Repositories\Category\CategoryRepository;

class CategoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {


    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->when(CategoryController::class)
            ->needs(RepositoryInterface::class)
            ->give(function() {
                return new CategoryRepository();
            });

        $this->app->when(BaseCategory::class)
            ->needs(RepositoryInterface::class)
            ->give(function() {
                return new CategoryRepository();
            });
    }
}