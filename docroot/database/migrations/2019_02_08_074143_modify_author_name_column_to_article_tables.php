<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyAuthorNameColumnToArticleTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Db::statement('ALTER TABLE article CHANGE COLUMN `authorName` `authorName` varchar(255) NULL;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Db::statement('ALTER TABLE article CHANGE COLUMN `authorName` `authorName` varchar(255) NOT NULL;');
    }
}
