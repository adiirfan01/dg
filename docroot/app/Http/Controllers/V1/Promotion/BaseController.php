<?php

namespace App\Http\Controllers\V1\Promotion;

use App\Http\Controllers\V1\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

class BaseController extends Controller
{
    public function getPromotionBySlug($slug, Request $request)
    {
        $promotion = $this->repo->getBySlug($request->get('locale'), $slug);

        return $this->promotionDataTransformer($this->getSuccess('success', $promotion));
    }

    public function promotionDataTransformer($data)
    {
        $data = get_object_vars($data);

        $finalized = [
            'articleId' => $data['original']['data']['articleId'],
            'image' => $data['original']['data']['image'],
            'createdDate' => $data['original']['data']['createdDate'],
            'slug' => $data['original']['data']['translation']['slug'],
            'title' => $data['original']['data']['translation']['title'],
        ];

        return new JsonResponse([
            'status' => [
                'code' => $this->code,
                'message' => $this->message
            ],
            'data' => $finalized
        ], $this->status);
    }

    public function getPromotionById($id, Request $request)
    {
        $promotion = $this->repo->findByArticleID($id, $request->get('locale'));

        return $this->promotionDataTransformer($this->getSuccess('success', $promotion));
    }

    public function getPromotionByTag(Request $request)
    {
        $promotion = $this->repo->findByArticleTag($request->get('tags'), $request->get('locale'));

        return $this->promotionMultiDataTransformer($this->getSuccess('success', $promotion));
    }

    public function promotionMultiDataTransformer($data)
    {

        $finalized = array();
        $data = get_object_vars($data);

        if ($data['original']['data'] != null) {
            foreach ($data['original']['data']['data'] as $artikel) {
                $tempData = [
                    'articleId' => $artikel['articleId'],
                    'image' => $artikel['image'],
                    'createdDate' => $artikel['createdDate'],
                    'slug' => $artikel['translation']['slug'],
                    'title' => $artikel['translation']['title'],
                ];
                array_push($finalized, $tempData);
            }
        } else {
            $finalized = null;
        }

        return new JsonResponse([
            'status' => [
                'code' => $this->code,
                'message' => $this->message
            ],
            'data' => $finalized
        ], $this->status);
    }

}