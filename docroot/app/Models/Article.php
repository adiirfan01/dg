<?php
/**
 * Created by PhpStorm.
 * User: Male
 */

namespace App\Models;

use App\Exceptions\ArticleStatusNotFoundException;
use App\Traits\ArticleImageTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Article extends Model
{
    use SoftDeletes;
    use ArticleImageTrait;

    protected $table = 'article';

    const CREATED_AT = 'createdDate';
    const UPDATED_AT = 'updatedDate';
    const DELETED_AT = 'deletedDate';

    /**
     * @var array
     */
    protected $fillable = [
        'authorId',
        'authorName',
        'publishedDate',
        'sticky',
        'tags',
        'status',
        'image',
        'categoryId',
        'inEditor',
        'createdBy',
        'createdDate',
        'updatedDate',
        'deletedDate',
        'updatedBy',
        'isDeleted'
    ];

    /**
     * Get the Content Translation
     */
    public function translation()
    {
        return $this->hasMany(ArticleTranslation::class, 'articleId');
    }

    /**
     * Get the Article Notes
     */
    public function notes()
    {
        return $this->hasMany(ArticleNotes::class, 'articleId');
    }

    /**
     * Get the Article Comment
     */
    public function comment()
    {
        return $this->hasMany(Comment::class, 'articleId');
    }

    /**
     * Get the Article Revision
     */
    public function revision()
    {
        return $this->hasMany(ArticleRevision::class, 'articleId');
    }

    /**
     * Get the Article Category
     */
    public function category()
    {
        return $this->belongsTo(Category::class, 'categoryId');
    }

    /**
     * Scope to filter tournament by status
     *
     * @param Builder $query
     * @param mixed $status
     * @return Builder $query
     */
    public function scopeFilterStatus($query, $status)
    {
        if (is_null($status)) return $query;

        try {
            $statusCode = [
                'published'      => 1,
                'draft'    => 0,
                'rejected'      => 4,
                'moderation'      => 2,
                'revision'      => 3,
            ][$status];
        } catch(\Exception $e) {
            throw new ArticleStatusNotFoundException();
        }

        return $query->where('status', $statusCode);
    }

    /**
     * Scope to filter tournament by highlight
     *
     * @param Builder $query
     * @param mixed $highlight
     * @return Builder $query
     */
    public function scopeFilterHighlight($query, $highlight)
    {
        if (is_null($highlight)) return $query;

        return $query->where('sticky', '=', $highlight);
    }
    public function scopeFilterCategory($query, $category)
    {
        if (is_null($category)) return $query;

        return $query->where('categoryId', '=', $category);
    }
}