<?php
/**
 * Created by PhpStorm.
 * User: salt
 * Date: 04/02/19
 * Time: 13:50
 */

namespace App\Http\Controllers\V1\Search;

use Illuminate\Http\Request;

class SearchController extends BaseController
{
    public function index()
    {
        $search = $this->repo->Search();
        return $this->getSearch('success',$search);
    }
}