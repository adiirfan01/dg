<?php

namespace App\Http\Request;

use App\Http\Request\FormRequest;
use Illuminate\Validation\Rule;

class StoreAnalyticRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'vasId' => 'required',
            'categoryId' => 'required',
            'type' => 'required',
            'typeVas' => 'required',
            'locale' => 'required'
        ];
    }


    protected function getValidatorInstance()
    {
        $this->getInputSource()->replace($this->modifyData());
        $validator = parent::getValidatorInstance();

        return $validator;
    }

    protected function modifyData()
    {
        $data = $this->validationData();
        return $data;
    }
}
