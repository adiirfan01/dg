<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateVideoCommentUserId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Db::statement('ALTER TABLE video_comment CHANGE COLUMN `userId` `userId` varchar(255) NOT NULL;');
        Db::statement('ALTER TABLE video_comment_reply CHANGE COLUMN `userId` `userId` varchar(255) NOT NULL;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('video_comment', function (Blueprint $table) {
            //
        });
    }
}
