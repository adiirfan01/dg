<?php

namespace App\Http\Controllers\V1\Revision;

use App\Http\Request\StoreRevisionRequest;
use App\Http\Request\UpdateRevisionRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\V1\Controller;

class RevisionController extends Controller
{
    public function index()
    {
        $video = $this->repo->all();
        return $this->getSuccess('success', $video);
    }
    public function save($videoId,StoreRevisionRequest $request)
    {
        $revision = $this->repo->addRevision($request->all(),$videoId);
        return $this->getSuccess('success', $revision);
    }
    public function update($id,UpdateRevisionRequest $request)
    {
        $request->merge([
            'id' => $id,
        ]);
        $revision = $this->repo->update($request->all(),$id);
        return $this->getSuccess('success', $revision);
    }
    public function resolve($id)
    {
        $data = [
            'id' => $id,
            'status' => '1',
        ];
        $revision = $this->repo->update($data,$id);
        return $this->getSuccess('success', $revision);
    }
    public function unresolve($id)
    {
        $data = [
            'id' => $id,
            'status' => '0',
        ];
        $revision = $this->repo->update($data,$id);
        return $this->getSuccess('success', $revision);
    }
}
