<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticle extends Migration
{
    private const ARTICLE_TABLE = 'article';
    private const ARTICLE_TRANSLATION_TABLE = "article_translation";
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::create(self::ARTICLE_TABLE, function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->bigInteger('authorId');
            $table->bigInteger('categoryId');
            $table->tinyInteger('inEditor')->default(0);
            $table->string('authorName', 255);
            $table->dateTime('publishedDate')->nullable(true);
            $table->tinyInteger('sticky')->default(0);
            $table->string('tags', 255);
            $table->tinyInteger('status')->default(0);
            $table->string('image', 255);
            $table->dateTime('createdDate');
            $table->string('createdBy', 255);
            $table->dateTime('updatedDate')->nullable(true);
            $table->string('updatedBy', 255)->nullable(true);
            $table->tinyInteger('isDeleted')->default(0);
        });

        Schema::create(self::ARTICLE_TRANSLATION_TABLE, function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->bigInteger('articleId');
            $table->string('title', 255);
            $table->string('slug', 255);
            $table->string('excerpt', 255);
            $table->text('content');
            $table->string('metaTitle', 255)->nullable(true);
            $table->string('metaDescription', 255)->nullable(true);
            $table->string('metaKeyword', 255)->nullable(true);
            $table->string('metaFbTitle', 255)->nullable(true);
            $table->string('metaFbDescription', 255)->nullable(true);
            $table->string('metaFbImage', 255)->nullable(true);
            $table->string('metaTwitterTitle', 255)->nullable(true);
            $table->string('metaTwitterDescription', 255)->nullable(true);
            $table->string('metaTwitterImage', 255)->nullable(true);
            $table->string('metaRobotsIndex', 255)->nullable(true);
            $table->string('metaRobotsAdvance', 255)->nullable(true);
            $table->string('metaRobotsFollow', 255)->nullable(true);
            $table->string('metaCanonicalUrl', 255)->nullable(true);
            $table->string('locale', 255);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(self::ARTICLE_TABLE);
        Schema::dropIfExists(self::ARTICLE_TRANSLATION_TABLE);
    }
}
