<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;

class VideoLikeComment extends Model
{


    protected $table = 'video_comment_like';
    protected $fillable = [
        'commentId',
        'userId',
        'type',
        'isDeleted'
    ];
}
