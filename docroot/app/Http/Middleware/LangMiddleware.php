<?php
/**
 * Created by PhpStorm.
 * User: Salt
 * Date: 22/12/2018
 * Time: 22:07
 */

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\JsonResponse;

class LangMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $responseCode = 401;

        if(!$request->hasHeader('accept-language')) return new JsonResponse([
            'status' => [
                'code'    => 1,
                'message' => "No Language provided",
                'errorMessage' => 'Error, Language Header not found'
            ]], $responseCode);


        $locale = $request->Header('accept-language');

        $request->request->add(['locale' => $locale]);

        return $next($request);
    }

}
