<?php

namespace App\Events\Video;

use Event;
use App\Models\Video;

class VideoUpdated extends Event
{
    public $video;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Video $video)
    {
        $this->video = $video;
    }
}
