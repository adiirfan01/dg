<?php
/**
 * Created by PhpStorm.
 * User: salt
 * Date: 04/02/19
 * Time: 12:15
 */

namespace App\Repositories\Article;

use App\Models\ArticleNotes;
use App\Repositories\RepositoryInterface;
use Illuminate\Database\Eloquent\Model;

class ReviewRepository implements RepositoryInterface
{
    public function all(array $columns = ['*'])
    {
        return ArticleNotes::all();
    }

    public function count() : int
    {
        return ArticleNotes::count();
    }

    public function paginate(int $perPage = 15, $columns = ['*'])
    {
        return null;
    }

    public function create(array $data): Model
    {
        $article = ArticleNotes::create($data);

        //event(new TournamentCreated($tournament, $data));

        return $article;
    }

    public function update(array $data, int $id)
    {
        $team = $this->find($id);

        $team->update($data);

        return $team->refresh();
    }

    public function updateBy(string $field, string $value, array $data)
    {
        $article = $this->findBy($field, $value);
        $article->update($data);

        return $article->refresh();
    }

    public function delete(int $id)
    {
        return null;
    }

    public function find(int $id, $columns = array('*'))
    {
        try {
            $article = ArticleNotes::findOrFail($id);
        }
        catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            throw new \App\Exceptions\ModelNotFoundException;
        }

        return $article;
    }

    public function findBy(string $field, string $value, $columns = ['*'])
    {
        try {
            $article = ArticleNotes::where($field, $value)->get();
        }
        catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            throw new \App\Exceptions\ModelNotFoundException;
        }

        return $article;
    }
}