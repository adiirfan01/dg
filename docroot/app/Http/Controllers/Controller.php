<?php

namespace App\Http\Controllers\V1;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\JsonResponse;
use League\Fractal\Serializer\ArraySerializer;
use Illuminate\Http\Request;
use League\Fractal\Manager;
use App\Repositories\RepositoryInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use stdClass;

class Controller extends BaseController
{
    //
    protected $auth0;
    protected $token;
    protected $repo;

    protected $fractal;

    protected $request;

    /**
     * @var
     */
    public $code;
    /**
     * @var
     */
    public $message;
    /**
     * @var
     */
    public $data;
    /**
     * @var
     */
    public $status;

    public function __construct(RepositoryInterface $repo, Manager $fractal, Request $request)
    {
        $this->repo = $repo;
        $this->fractal = $fractal;
        $this->request = $request;
    }

    /**
     * @param $code
     */
    public function setCode($code)
    {
        $this->code = (int)$code;
    }

    /**
     * @param $data
     */
    public function setData($data = null)
    {
        //$this->data = $this->transformToArray($data);
        $this->data = $data;
    }

    /**
     * @param $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @param $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @param $token
     */
    public function setToken($token)
    {
        $this->token = $token;
    }

    public function getReturn()
    {
        return new JsonResponse([
            'status' => [
                'code' => $this->code,
                'message' => $this->message
            ],
            'data' => $this->data
        ], $this->status);
    }

    public function getSuccess($message = null, $data = null, $status = 200)
    {
        $this->setCode(env('STATUS_SUCCESS_CODE'));
        if ($data != null) {
            $this->setData($this->transformToArray($data));
        }

        if ($message != null) {
            $this->setMessage($message);
        }

        $this->setStatus($status);

        return $this->getReturn();
    }

    public function getError($message = null, $data = null, $status = 400)
    {
        $this->setCode(env('STATUS_FAILED_CODE'));
        if ($data != null) {
            $this->setData($data);
        }

        if ($message != null) {
            $this->setMessage($message);
        } else {
            $this->setMessage("Caught Error Exception!");
        }

        $this->setStatus($status);

        return $this->getReturn();
    }

    public function getErrorValidation($message = null, $data = null, $status = 422)
    {
        $this->setCode(env('STATUS_FAILED_CODE'));
        if ($data != null) {
            $this->setData($data);
        }

        if ($message != null) {
            $this->setMessage($message);
        }

        $this->setStatus($status);

        return $this->getReturn();
    }

    private function transformToArray($data)
    {
        $paginator = null;

        if (is_array($data)) return $data;

        //Replace paginator data to collection
        if ($data instanceof LengthAwarePaginator) {
            $paginator = $data; //Copy data into paginator before replace it with collection
            $data = $data->getCollection();
        }

        if ($this->isNoNeedToTransform($data)) return [];

        $resourceType = 'League\\Fractal\\Resource\\';
        $resourceType .= $data instanceof Model ? 'Item' : 'Collection';
        $transformerClass = $this->getTransformer($data);
        $resource = new $resourceType($data, $transformerClass);
        //Set paginator if there is page param
        if ($this->request->get('page') && $paginator instanceof LengthAwarePaginator) {
            $resource->setPaginator(new IlluminatePaginatorAdapter($paginator));
        }

        if (is_null($this->request->get('include'))) {
            return $this->fractal->setSerializer(new ArraySerializer)->createData($resource)->toArray();
        }

        return $this->fractal->parseIncludes($this->request->get('include'))->setSerializer(new ArraySerializer)->createData($resource)->toArray();
    }

    private function getTransformer($obj)
    {
        $temp = explode('\\', get_class($obj));
        $transformerClass = end($temp);
        if ($transformerClass == 'Collection') return $this->getTransformer($obj->first());

        $transformerClass = 'App\\Article\\Transformer\\' . $transformerClass . 'Transformer';

        if ($transformerClass == \App\Article\Transformer\ArticleTransformer::class) {
            return new $transformerClass(app()->make('request')->get('locale'));
        }

        if ($transformerClass == \App\Article\Transformer\CategoryTransformer::class) {
            return new $transformerClass(app()->make('request')->get('locale'));
        }

        return new $transformerClass;
    }

    /**
     * Check the data if no need to transform
     *
     * @param Model|Collection $data
     * @return boolean
     */
    private function isNoNeedToTransform($data)
    {
        //Model always need to transform
        if ($data instanceof Model) return false;

        //If Collection is empty, no need to transform
        return $data->count() == 0;
    }

    public function getAll($message = null, $query = null, $status = 200, $div = false)
    {
        $this->setCode(env('STATUS_SUCCESS_CODE'));
        if ($message != null) {
            $this->setMessage($message);
        }
        $this->setStatus($status);
        $article = $this->transformToArray($query['article']);
        $video = $this->transformToArray($query['video']);
        $data = $this->serializeAll($article, $video, $div);

        return new JsonResponse([
            'status' => [
                'code' => $this->code,
                'message' => $this->message,
            ],
            'data' => $data
        ], $this->status);
    }

    public function serializeAll($article, $video, $div)
    {
        $data['data'] = array();
        if (isset($article['data'])) {
            foreach ($article['data'] as $article) {
                if (!isset($article['translation']['slug'])) {
                    continue;
                }
                $data['data'][] = [
                    'id' => $article['articleId'],
                    'author' => $article['author'],
                    'image' => $article['image'],
                    'category' => $article['category'],
                    'publishedDate' => $article['publishedDate'],
                    'title' => $article['translation']['title'],
                    'slug' => $article['translation']['slug'],
                    'content' => $article['translation']['excerpt'],
                    'highlight' => $article['highlight'],
                    'isBookmarked' => $article['isBookmarked'],
                    'type' => 'article',
                ];
            }
        }
        if (isset($video['data'])) {
            foreach ($video['data'] as $video) {
                if (!isset($video['detail']['slug'])) {
                    continue;
                }
                $data['data'][] = [
                    'id' => $video['videoId'],
                    'author' => $video['author'],
                    'category' => $video['category'],
                    'publishedDate' => $video['publishedDate'],
                    'title' => $video['detail']['title'],
                    'content' => $video['detail']['desc'],
                    'slug' => $video['detail']['slug'],
                    'highlight' => $video['highlight'],
                    'videoUrl' => $video['videoUrl'],
                    'isBookmarked' => $video['isBookmarked'],
                    'type' => 'video',
                ];
            }
        }
        if ($div) {
            $data = $this->dividePerSection($data['data']);
        }

        return $data;
    }

    public function dividePerSection($data)
    {
        $null = new stdClass();
        $array = array();
        $array['section2'] = array();
        $array['section5'] = array();
        $array['section1'] = (isset($data[0])) ? $data[0] : $null;
        for ($x = 1; $x <= 4; $x++) {
            $array['section2'][] = (isset($data[$x])) ? $data[$x] : $null;
        }
        $array['section3'] = (isset($data[5])) ? $data[5] : $null;
        $array['section4'] = (isset($data[6])) ? $data[6] : $null;
        for ($x = 7; $x <= 9; $x++) {
            $array['section5'][] = (isset($data[$x])) ? $data[$x] : $null;
        }
        $array['section6'] = (isset($data[10])) ? $data[10] : $null;

        return $array;
    }

    public function getPopular($message = null, $data = null, $status = 200)
    {
        $this->setCode(env('STATUS_SUCCESS_CODE'));
        if ($data != null) {
            $data = $this->transformToArray($data);
        }

        if ($message != null) {
            $this->setMessage($message);
        }
        $data = $this->dividePerSection($data['data']);
        $this->setStatus($status);

        return new JsonResponse([
            'status' => [
                'code' => $this->code,
                'message' => $this->message,
            ],
            'data' => $data
        ], $this->status);
    }
    public function getSearch($message = null, $query = null, $status = 200)
    {
        $this->setCode(env('STATUS_SUCCESS_CODE'));
        if ($message != null) {
            $this->setMessage($message);
        }
        $this->setStatus($status);
        $article= $this->transformToArray($query['article']);
        $video = $this->transformToArray($query['video']);
        $data = $this->serializeAll($article, $video, false);
        return new JsonResponse([
            'status' => [
                'code' => $this->code,
                'message' => $this->message,
            ],
            'data' => $data
        ], $this->status);
    }

}
