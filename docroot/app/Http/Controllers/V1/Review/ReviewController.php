<?php
/**
 * Created by PhpStorm.
 * User: salt
 * Date: 04/02/19
 * Time: 13:51
 */

namespace App\Http\Controllers\V1\Review;

use App\Http\Controllers\V1\Review;
use App\Http\Request\StoreEditNotesRequest;
use App\Http\Request\StoreNotesRequest;
use Illuminate\Http\Request;

class ReviewController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notes = $this->repo->all();

        return $this->getSuccess('success', $notes);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save($articleId, StoreNotesRequest $request)
    {
        $request->merge([
            'articleId' => $articleId
        ]);
        $notes = $this->repo->create($request->all());

        return $this->getSuccess('success', $notes);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($articleId, $id, StoreEditNotesRequest $request)
    {
        $request->merge([
            'articleId' => $articleId
        ]);
        $notes = $this->repo->update($request->all(), $id);

        return $this->getSuccess('success', $notes);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $this->repo->delete($id);

        return $this->getSuccess('deleted');
    }
}