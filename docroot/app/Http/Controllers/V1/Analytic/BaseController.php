<?php
/**
 * Created by PhpStorm.
 * User: salt
 * Date: 04/02/19
 * Time: 13:50
 */

namespace App\Http\Controllers\V1\Analytic;

use App\Http\Controllers\V1\Controller;

class BaseController extends Controller
{
    public function getDataWeekly()
    {
        $data = $this->repo->dateRange(1);
        return $this->getSuccess('success', $data);
    }
    public function Popular()
    {
        $data = $this->repo->dateRange(1);
        return $this->getPopular('success', $data);
    }
}