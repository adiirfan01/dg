<?php
/**
 * Created by PhpStorm.
 * User: salt
 * Date: 04/02/19
 * Time: 12:15
 */

namespace App\Repositories\Category;

use App\Models\Category;
use App\Models\CategoryTranslation;
use App\Repositories\Category\CategoryTranslationRepository as Translation;
use App\Repositories\RepositoryInterface;
use Illuminate\Database\Eloquent\Model;
use \Cviebrock\EloquentSluggable\Services\SlugService;

class CategoryRepository implements RepositoryInterface
{
    public function all(array $columns = ['*'])
    {
        $locale = app('request')->input('locale');
        $categoryTrans = CategoryTranslation::where('locale', $locale)->get();
        $categoryid = array_map(function ($object) { return $object->categoryId; }, json_decode($categoryTrans));
        $category = Category::whereIn('id', $categoryid)->get();

        return $category;
    }

    public function count() : int
    {
        return Category::count();
    }

    public function paginate(int $perPage = 15, $columns = ['*'])
    {
        return null;
    }

    public function create(array $data): Model
    {
        $data['slug'] = SlugService::createSlug(Category::class, 'slug', $data['name'], ['unique' => true]);
        $category = Category::create($data);

        $categoryData['categoryId'] = $category->id;
        $data = array_merge($data, $categoryData);

        $translation = new Translation();
        $translation->create($data);

        return $category;
    }

    public function update(array $data, int $id)
    {
        $category = $this->find($id);
        $category->update($data);

        $categoryData['categoryId'] = $category->id;
        $data = array_merge($data, $categoryData);

        $getDataTranslation = CategoryTranslation::where(["categoryId" => $category->id, 'locale' => $data['locale']])->first();
        $translation = new Translation();

        if($getDataTranslation){
            $translation->update($data, $getDataTranslation->id);
        }else{
            $translation->create($data);
        }

        return $category->refresh();
    }

    public function updateBy(string $field, string $value, array $data)
    {
        $category = $this->findBy($field, $value)->first();
        $category->update($data);

        $categoryData['categoryId'] = $category->id;
        $data = array_merge($data, $categoryData);

        $getDataTranslation = CategoryTranslation::where(["categoryId" => $category->id, 'locale' => $data['locale']])->first();
        $translation = new Translation();

        if($getDataTranslation){
            $translation->update($data, $getDataTranslation->id);
        }else{
            $translation->create($data);
        }

        return $category->refresh();
    }

    public function delete(int $id)
    {
        return null;
    }

    public function find(int $id, $columns = array('*'))
    {
        try {
            $category = Category::findOrFail($id);
        }
        catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            throw new \App\Exceptions\ModelNotFoundException;
        }

        return $category;
    }

    public function findBy(string $field, string $value, $columns = ['*'])
    {
        try {
            $category = Category::where($field, $value)->get();
        }
        catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            throw new \App\Exceptions\ModelNotFoundException;
        }

        return $category;
    }
}