<?php
/**
 * Created by PhpStorm.
 * User: salt
 * Date: 04/02/19
 * Time: 13:51
 */

namespace App\Http\Controllers\V1\Comment;

use App\Http\Controllers\V1\Comment;
use App\Http\Request\StoreCommentRequest;
use App\Http\Request\StoreEditCommentRequest;
use Illuminate\Http\Request;

class CommentController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($articleId)
    {
        $comment = $this->repo->getBy('articleId',$articleId);

        if(count($comment)){
            return $this->getSuccess('success', $comment);
        }

        return $this->getSuccess('success', null);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save($articleId, StoreCommentRequest $request)
    {
        $request->merge([
            'articleId' => $articleId
        ]);

        $comment = $this->repo->create($request->all());

        return $this->getSuccess('success', $comment);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($articleId, $id, StoreEditCommentRequest $request)
    {
        $request->merge([
            'articleId' => $articleId
        ]);

        $comment = $this->repo->update($request->all(), $id);

        return $this->getSuccess('success', $comment);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $this->repo->delete($id);

        return $this->getSuccess('Deleted');
    }
    public function like($id,Request $request)
    {
        $comment = $this->repo->like($request->all(), $id,1);
        return $this->getSuccess('success', $comment);
    }
    public function dislike($id,Request $request)
    {
        $comment = $this->repo->like($request->all(), $id,0);
        return $this->getSuccess('success', $comment);
    }
    public function unlike($id,Request $request)
    {
        $comment = $this->repo->unlike($request->all(), $id);
        return $this->getSuccess('success', $comment);
    }
}