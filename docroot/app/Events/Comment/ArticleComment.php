<?php

namespace App\Events\Comment;

use Event;
use App\Models\Comment;

class ArticleComment extends Event
{
    public $video;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Comment $articleComment)
    {
        $this->articleComment = $articleComment;
    }
}
