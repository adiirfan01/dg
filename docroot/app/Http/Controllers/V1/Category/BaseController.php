<?php
/**
 * Created by PhpStorm.
 * User: salt
 * Date: 04/02/19
 * Time: 13:50
 */

namespace App\Http\Controllers\V1\Category;

use App\Http\Controllers\V1\Controller;

class BaseController extends Controller
{
    public function getCategoryBySlug($slug)
    {
       $category = $this->repo->findBy('slug', $slug)->first();

       return $this->getSuccess('success', $category);
    }
}