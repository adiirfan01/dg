<?php

namespace App\Http\Controllers\V1\Media;

use App\Http\Request\StoreCommentRequest;
use App\Http\Request\StoreEditCommentRequest;
use App\Http\Request\StoreMediaRequest;
use Illuminate\Http\Request;

class MediaController extends BaseController
{
    public function store(StoreMediaRequest $request){

        $media = $this->repo->create($request->all());
        return $this->getSuccess('success',$media);
    }
    public function update($uuid,StoreMediaRequest $request){
        $media = $this->repo->updateImage($request->all(),$uuid);

        return $this->getSuccess('success',$media);
    }
    public function remove($uuid){
        $media = $this->repo->deleteImage($uuid);
        if($media){
            return $this->getSuccess('gambar berhasil dihapus');
        }
        return $this->getError('gambar tidak berhasil dihapus');
    }
    public function index(){
        $media = $this->repo->all();
        return $this->getSuccess('success',$media);
    }
}
