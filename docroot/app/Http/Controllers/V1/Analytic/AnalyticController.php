<?php
/**
 * Created by PhpStorm.
 * User: salt
 * Date: 04/02/19
 * Time: 13:50
 */

namespace App\Http\Controllers\V1\Analytic;

use App\Http\Controllers\V1\Analytic;
use App\Http\Request\StoreAnalyticRequest;

class AnalyticController extends BaseController
{
    public function store(StoreAnalyticRequest $request)
    {
        $this->repo->store($request->all());

        return $this->getSuccess('success');
    }
}