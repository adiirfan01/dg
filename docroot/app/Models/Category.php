<?php
/**
 * Created by PhpStorm.
 * User: salt
 * Date: 08/11/18
 * Time: 11:48
 */
namespace App\Models;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Cviebrock\EloquentSluggable\Sluggable;

/**
 * @property int $id
 * @property string $slug
 * @property \Carbon\Carbon $createdDate
 * @property string $createdBy
 * @property \Carbon\Carbon $updatedDate
 * @property string $updatedBy
 * @property boolean $isDeleted
 */

class Category extends Model
{
    use Sluggable;
    use SoftDeletes;

    protected $table = 'category';

    const CREATED_AT = 'createdDate';
    const UPDATED_AT = 'updatedDate';
    const DELETED_AT = 'deletedDate';

    /**
     * @var array
     */
    protected $fillable = [
        'slug',
        'createdBy',
        'updatedBy',
        'createdDate',
        'updatedDate',
        'deletedDate',
        'isDeleted'
    ];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    /**
     * Get the Article
     */
    public function article()
    {
        return $this->hasMany(Article::class, 'categoryId');
    }


    /**
     * Get the Content Translation
     */
    public function translation()
    {
        return $this->hasMany(CategoryTranslation::class, 'categoryId');
    }

}