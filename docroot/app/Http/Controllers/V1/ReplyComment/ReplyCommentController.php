<?php
/**
 * Created by PhpStorm.
 * User: salt
 * Date: 04/02/19
 * Time: 14:42
 */

namespace App\Http\Controllers\V1\ReplyComment;

use App\Http\Controllers\V1\ReplyComment;
use App\Http\Request\StoreCommentRequest;
use Illuminate\Http\Request;

class ReplyCommentController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $replyComment = $this->repo->all();

        return $this->getSuccess('success', $replyComment);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save($commentId, StoreCommentRequest $request)
    {
        $request->merge([
            'commentId' => $commentId
        ]);
        $replyComment = $this->repo->create($request->all());

        return $this->getSuccess('success', $replyComment);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($commentId, $id, Request $request)
    {
        $request->merge([
            'commentId' => $commentId
        ]);
        $replyComment = $this->repo->update($request->all(), $id);

        return $this->getSuccess('success', $replyComment);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $this->repo->delete($id);

        return $this->getSuccess('deleted');
    }
}