<?php
/**
 * Created by PhpStorm.
 * User: salt
 * Date: 04/02/19
 * Time: 12:15
 */

namespace App\Repositories\Tags;

use App\Models\Tags;
use App\Repositories\RepositoryInterface;
use Illuminate\Database\Eloquent\Model;
use \Cviebrock\EloquentSluggable\Services\SlugService;

class TagsRepository implements RepositoryInterface
{
    public function all(array $columns = ['*'])
    {
        return Tags::get();
    }

    public function count() : int
    {
        return Tags::count();
    }

    public function paginate(int $perPage = 15, $columns = ['*'])
    {
        return null;
    }

    public function create(array $data): Model
    {
        $data['slug'] = SlugService::createSlug(Tags::class, 'slug', $data['slug'], ['unique' => true]);
        $tags = Tags::create($data);

        return $tags;
    }

    public function update(array $data, int $id)
    {
        $data['slug'] = SlugService::createSlug(Tags::class, 'slug', $data['slug'], ['unique' => true]);
        $tags = $this->find($id);
        $tags->update($data);

        return $tags->refresh();
    }

    public function updateBy(string $field, string $value, array $data)
    {
        $tags = $this->findBy($field, $value)->first();
        if(empty($tags)){
            throw new \App\Exceptions\ModelNotFoundException;
        }

        $data['slug'] = SlugService::createSlug(Tags::class, 'slug', $data['slug'], ['unique' => true]);
        $tags->update($data);

        return $tags->refresh();
    }

    public function delete(int $id)
    {
        return null;
    }

    public function find(int $id, $columns = array('*'))
    {
        try {
            $tags = Tags::findOrFail($id);
        }
        catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            throw new \App\Exceptions\ModelNotFoundException;
        }

        return $tags;
    }

    public function findBy(string $field, string $value, $columns = ['*'])
    {
        try {
            $tags = Tags::where($field, $value)->get();
            if(empty($tags)){
                throw new \App\Exceptions\ModelNotFoundException;
            }
        }
        catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            throw new \App\Exceptions\ModelNotFoundException;
        }

        return $tags;
    }
}