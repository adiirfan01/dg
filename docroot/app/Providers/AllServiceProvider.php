<?php
/**
 * Created by PhpStorm.
 * User: salt
 * Date: 31/01/19
 * Time: 16:31
 */
namespace App\Providers;

use App\Http\Controllers\V1\All\{AllController, BaseController as BaseAll};
use Illuminate\Support\ServiceProvider;
use App\Repositories\RepositoryInterface;
use App\Repositories\All\{
    AllRepository
};
class AllServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {


    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->when(AllController::class)
            ->needs(RepositoryInterface::class)
            ->give(function() {
                return new AllRepository();
            });
        $this->app->when(BaseAll::class)
            ->needs(RepositoryInterface::class)
            ->give(function() {
                return new AllRepository();
            });
    }
}
