<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideosCommentReply extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('video_comment_reply', function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->bigInteger('commentId');
            $table->string('userId');
            $table->bigInteger('like')->default(0);;
            $table->bigInteger('dislike')->default(0);;
            $table->text('comment');
            $table->dateTime('createdDate');
            $table->string('createdBy', 255);
            $table->dateTime('updatedDate')->nullable(true);
            $table->string('updatedBy', 255)->nullable(true);
            $table->tinyInteger('isDeleted')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('video_comment_reply');
    }
}
