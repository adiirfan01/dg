<?php
/**
 * Created by PhpStorm.
 * User: salt
 * Date: 04/02/19
 * Time: 14:44
 */

namespace App\Repositories\Article;

use App\Models\CommentReply;
use App\Repositories\RepositoryInterface;
use Illuminate\Database\Eloquent\Model;

class ReplyCommentRepository implements RepositoryInterface
{
    public function all(array $columns = ['*'])
    {
        return null;
    }

    public function count() : int
    {
        return CommentReply::count();
    }

    public function paginate(int $perPage = 15, $columns = ['*'])
    {
        return null;
    }

    public function create(array $data): Model
    {
        $replyComment = CommentReply::create($data);

        return $replyComment;
    }

    public function update(array $data, int $id)
    {
        $replyComment = $this->find($id);

        $replyComment->update($data);

        return $replyComment->refresh();
    }

    public function updateBy(string $field, string $value, array $data)
    {
        $replyComment = $this->findBy($field, $value);
        $replyComment->update($data);

        return $replyComment->refresh();
    }

    public function delete(int $id)
    {
        return null;
    }

    public function find(int $id, $columns = array('*'))
    {
        try {
            $replyComment = CommentReply::findOrFail($id);
        }
        catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            throw new \App\Exceptions\ModelNotFoundException;
        }

        return $replyComment;
    }

    public function findBy(string $field, string $value, $columns = ['*'])
    {
        try {
            $replyComment = CommentReply::where($field, $value)->get();
        }
        catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            throw new \App\Exceptions\ModelNotFoundException;
        }

        return $replyComment;
    }
}