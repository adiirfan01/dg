<?php

namespace App\Http\Request;
use App\Models\Category;
use App\Models\Tags;

class StoreDraftRequest extends FormRequest
{
    protected function validationData()
    {
        $data = $this->merge($this->all());
        return $data->request->all();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'userId' => 'required',
        ];
    }


    protected function getValidatorInstance()
    {
        $this->getInputSource()->replace($this->modifyData());
        $validator = parent::getValidatorInstance();

        return $validator;
    }

    protected function modifyData()
    {
        $data = $this->validationData();

        if(array_key_exists('userId', $data)){
            $data['createdBy'] = $data['userId'];
            $data['authorId'] = $data['userId'];
        }else{
            $data['createdBy'] = "sms|5c6e37069e6bbdc8bc73fa9c";
            $data['authorId'] = "sms|5c6e37069e6bbdc8bc73fa9c";
        }
        if(!array_key_exists('title',$data)){
            $data['title'] = "Draft";
        }
        if(!array_key_exists('desc',$data)){
            $data['desc'] = " ";
        }
        if(!array_key_exists('excerpt',$data)){
            $data['excerpt'] = " ";
        }
        if(!array_key_exists('content',$data)){
            $data['content'] = " ";
        }
        if(!array_key_exists('videoUrl',$data)){
            $data['videoUrl'] = " ";
        }
        if(!array_key_exists('image',$data)){
            $data['image'] = " ";
        }
        if(array_key_exists('categoryId', $data)){
            try {
                $category = Category::findOrFail($data['categoryId']);
            }
            catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
                throw new \App\Exceptions\CategoryNotFoundException();
            }
        }

        if(array_key_exists('tags',$data)){
            $tags = $data['tags'];
            $tags = substr($tags, 1);
            $tags = substr($tags, 0, -1);
            $tags = explode(",", $tags);
            for ($i = 0; $i < count($tags); $i++) {
                try {
                    Tags::findOrFail($tags[$i]);
                }catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
                    throw new \App\Exceptions\TagsNotFoundException();
                }
            }
        }

        return $data;
    }
}
