<?php
/**
 * Created by PhpStorm.
 * User: salt
 * Date: 31/01/19
 * Time: 16:31
 */
namespace App\Providers;

use App\Http\Controllers\V1\Search\{SearchController, BaseController as BaseSearch};
use Illuminate\Support\ServiceProvider;
use App\Repositories\RepositoryInterface;
use App\Repositories\Search\{
    SearchRepository
};
class SearchServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {


    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->when(SearchController::class)
            ->needs(RepositoryInterface::class)
            ->give(function() {
                return new SearchRepository();
            });
        $this->app->when(BaseSearch::class)
            ->needs(RepositoryInterface::class)
            ->give(function() {
                return new SearchRepository();
            });
    }
}
