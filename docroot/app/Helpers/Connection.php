<?php
/**
 * Created by PhpStorm.
 * User: salt
 * Date: 21/01/19
 * Time: 10:34
 */

namespace App\Helpers;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\JsonResponse;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\MessageFormatter;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\FirePHPHandler;
use Monolog\Logger;

class Connection
{
    public function getRequest($url, $userId, $nameLog)
    {
        $logger = new Logger($nameLog);
        $logger->pushHandler(new StreamHandler(storage_path("logs/".date('d') . date('m') . date("Y") ."-".$nameLog)), Logger::DEBUG);
        $stack = HandlerStack::create();
        $stack->push(Middleware::log(
            $logger,
            new MessageFormatter('{url} - {req_body} - {res_body}')
        ));

        $client = new Client([
            "base_uri" => $url,
            "verify" => false,
            'handler' => $stack,
        ]);

        try{
            $getData = $client->request('GET', $url, [
                'query' => ['userId' => $userId],
                'headers' => ['x-api-key' => getenv('API_KEY_USER')]
            ]);
        }catch (\Exception $e) {
            return new JsonResponse([
                'status' => [
                    'code'    => 1,
                    'message' => $e->getMessage()
                ]
            ], 400);
        }

        return $this->getData($getData);

    }

    public function getData($data)
    {
        return json_decode($data->getBody());
    }
}