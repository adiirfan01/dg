<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 09/05/2019
 * Time: 13:13
 */
namespace App\Helpers;

use App\Models\Media;
use Illuminate\Support\Facades\Cache;

class attachmentConverter
{
    public function getAttachment($paragraph,$id){
        $key = 'article_translation_'.$id;
        if(Cache::has($key)){
            return Cache::get($key);
        }
        preg_match_all('#\{(.*?)\}#', $paragraph, $match);
        $attachment = $match[0];
        $attachment_data = $match[1];
        $x = 0;
        foreach ($attachment_data as $attachment_data) {
            $data = explode(" ", $attachment_data);
            if ($data[0] == 'image') {
                $date = $this->cleanup($data[1]);
                $attribute = [
                    $date[0] => $date[1]
                ];
                $data = $this->translateImage($attribute['id']);
                $paragraph = str_replace($attachment[$x], $data, $paragraph);
            }
            $x++;
        }
        Cache::store('redis')->put($key, $paragraph,'60');
        return $paragraph;
    }
    public function translateImage($uuid){
        $media = Media::query()->where('uuid',$uuid)->first();
        $url = url('/').'/'.$media->url;
        $html = "<img src=\"".$url."\">";
        return $html;
    }
    public function cleanup($char){
        $char = explode(' ',trim(preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9- ]/', ' ', urldecode(html_entity_decode($char))))));
        return $char;
    }
}
