<?php
/**
 * Created by PhpStorm.
 * User: Male
 */

namespace App\Models;

use App\Exceptions\ArticleStatusNotFoundException;
//use Illuminate\Support\Facades\Cache;
//use Illuminate\Support\Facades\Redis;
use Illuminate\Database\Eloquent\Model;
//use Illuminate\Support\Facades\DB;
//use Ramsey\Uuid\Uuid;

class Video extends Model
{

    protected $table = 'video';

    const CREATED_AT = 'createdDate';
    const UPDATED_AT = 'updatedDate';

    /**
     * @var array
     */
    protected $fillable = [
        'authorId',
        'publishedDate',
        'videoUrl',
        'highlight',
        'totalViews',
        'status',
        'categoryId',
        'inEditor',
        'createdBy',
        'createdDate',
        'updatedDate',
        'updatedBy',
        'isDeleted',
        'tags',
        'video_type',
    ];

    /**
     * Get the Content Translation
     */
    public function detail()
    {
        return $this->hasMany(VideoDetail::class, 'videoId');
    }
    public function revision()
    {
        return $this->hasMany(Revision::class,'videoId');
    }

    /**
     * Get the Video Comment
     */
    public function comment()
    {
        return $this->hasMany(VideoComment::class, 'videoId');
    }


    /**
     * Get the Video Category
     */
    public function category()
    {
        return $this->belongsTo(Category::class, 'categoryId');
    }

    /**
     * Scope to filter tournament by status
     *
     * @param Builder $query
     * @param mixed $status
     * @return Builder $query
     */
    public function scopeFilterStatus($query, $status)
    {
        if (is_null($status)) return $query;

        try {
            $statusCode = [
                'published'      => 1,
                'draft'    => 0,
                'rejected'      => 4,
                'moderation'      => 2,
                'revision'      => 3,
            ][$status];
        } catch(\Exception $e) {
            throw new ArticleStatusNotFoundException();
        }

        return $query->where('status', $statusCode);
    }

    /**
     * Scope to filter tournament by highlight
     *
     * @param Builder $query
     * @param mixed $highlight
     * @return Builder $query
     */
    public function scopeFilterHighlight($query, $highlight)
    {
        if (is_null($highlight)) return $query;

        return $query->where('highlight', '=', $highlight);
    }
    public function scopeRelated($query, $categoryId, $id)
    {
        if (is_null($categoryId)) return $query;

        return $query->where('categoryId', '=', $categoryId)->where('id','!=',$id)->limit(5);
    }
    public function scopeUser($query, $authorId)
    {
        if (is_null($authorId)) return $query;

        return $query->where('authorId', '=', $authorId);
    }
    public function scopeUserPref($query, $userpref)
    {
        if (is_null($userpref)) return $query;
        foreach($userpref as $pref){
            $query->orWhere('tags', 'LIKE', '%'.$pref.'%');
        }
        return $query;
    }
    public function scopeFilterCategory($query, $category)
    {
        if (is_null($category)) return $query;

        return $query->where('categoryId', '=', $category);
    }
    public function scopeFilterType($query, $type)
    {
        if (is_null($type)) return $query;

        return $query->where('video_type', '=', $type);
    }

}