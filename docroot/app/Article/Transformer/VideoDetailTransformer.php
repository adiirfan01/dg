<?php
/**
 * Created by PhpStorm.
 * User: salt
 * Date: 04/02/19
 * Time: 13:46
 */

namespace App\Article\Transformer;

use App\Models\VideoDetail;
use League\Fractal\TransformerAbstract;

class VideoDetailTransformer extends TransformerAbstract
{
    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(VideoDetail $detail)
    {
        return [
            'title' => $detail->title,
            'desc' => $detail->desc,
            'slug' => $detail->slug,
            'locale' => $detail->locale,
        ];
    }
}