<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Category::class, 10)->create()->each(function ($category) {
            $category->translation()->saveMany(factory(App\Models\CategoryTranslation::class, 1)->make());
        });
        factory(App\Models\Video::class, 10)->create()->each(function ($video) {
            $video->detail()->saveMany(factory(App\Models\VideoDetail::class, 1)->make());
        });
    }
}
