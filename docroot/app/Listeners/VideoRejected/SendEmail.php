<?php

namespace App\Listeners\VideoCreated;

use App\Events\Video\VideoRejected;


/**
 * Class: SendEmail
 *
 * Send email to all member after joining the tournament
 */
class SendEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  EntrylistAdded  $event
     * @return void
     */
    public function handle(VideoRejected $event)
    {
        $authorId = $event->video->authorId ?? null;

        if($authorId){
            $user = $this->getUser($authorId);

            // TODO email user using data from
        }
    }
    public function getUser($authorId){
        //TODO grab userdata from user Service
        return null;
    }
}
