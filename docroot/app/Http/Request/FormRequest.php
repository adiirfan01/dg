<?php

namespace App\Http\Request;

use Pearl\RequestValidate\RequestAbstract;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\JsonResponse;

class FormRequest extends RequestAbstract
{
    protected function validationData()
    {
        $data = $this->merge($this->all());
        return $data->request->all();
    }

    /**
     * Format the errors from the given Validator instance.
     *
     * @param  \Illuminate\Contracts\Validation\Validator  $validator
     * @return \Illuminate\Http\JsonResponse
     */
    protected function formatErrors(Validator $validator)
    {
        return new JsonResponse([
            'status' => [
                'code'    => 1,
                'message' => $validator->getMessageBag()->first()
            ]
        ], 422);
    }
}
