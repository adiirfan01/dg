<?php
/**
 * Created by PhpStorm.
 * User: maleakhi
 * Date: 3/4/19
 * Time: 3:34 PM
 */

use App\Models\Article;
use App\Events\Article\ArticleCreated;
use App\Events\Article\ArticlePublished;
use App\Events\Article\ArticleUpdated;

class ArticleObserver{
    public function creating(Article $article){
        //article Create
        event(new ArticleCreated($article));
    }
    public function updating(Article $article){
        if($article->isDirty('publishedDate')){
            //article published
            event(new ArticlePublished($article));
        }else{
            //article Update
            event(new ArticleUpdated($article));
        }
    }
}