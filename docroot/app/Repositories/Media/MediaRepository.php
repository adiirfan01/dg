<?php
/**
 * Created by PhpStorm.
 * User: salt
 * Date: 04/02/19
 * Time: 12:14
 */

namespace App\Repositories\Media;

use App\Models\Media;
use App\Repositories\RepositoryInterface;
use Illuminate\Database\Eloquent\Model;
use \Cviebrock\EloquentSluggable\Services\SlugService;
use phpDocumentor\Reflection\Types\Integer;
use App\Article\ImageSave;

class MediaRepository implements RepositoryInterface
{
    public function all(array $columns = ['*']){
        $userId = app('request')->input('userId');
        $media = Media::query()->where('userId',$userId)->get();
        return $media;
    }

    public function count() : int
    {
        return null;
    }

    public function paginate(int $perPage = 15, $columns = ['*'])
    {
        return null;
    }

    public function create(array $data): Model
    {
        $path = ImageSave::save($data['image']);
        $data['url'] = $path;
        $media = Media::create($data);
        return $media;
    }
    public function updateImage(array $data, string $uuid)
    {
        $media = Media::query()->where('uuid',$uuid)->first();
        if($media){
            $delete = ImageSave::delete($media->url);
            if($delete){
                $path = ImageSave::save($data['image']);
                $data['url'] = $path;
                Media::where('uuid',$uuid)->
                update([
                    'url' => $path,
                ]);
                return $media->refresh();
                //return $delete;
            }
        }
        else null;
    }
    public function deleteImage(string $uuid){
        $media = Media::query()->where('uuid',$uuid)->first();
        if($media){
            $delete = ImageSave::delete($media->url);
            if($delete){
                Media::where('uuid',$uuid)->delete();
                return true;
            }
            return false;
        }
        return false;
    }
    public function update(array $data, int $id)
    {
        return null;
    }

    public function updateBy(string $field, string $value, array $data)
    {
       return null;
    }

    public function delete(int $id)
    {
        return null;
    }

    public function find(int $id, $columns = array('*'))
    {
        return null;
    }

    public function findBy(string $field, string $value, $columns = ['*'])
    {
       return null;
    }
}