<?php
/**
 * Created by PhpStorm.
 * User: salt
 * Date: 04/02/19
 * Time: 12:14
 */

namespace App\Repositories\Article;

use App\Models\ArticleRevision;
use App\Repositories\RepositoryInterface;
use Illuminate\Database\Eloquent\Model;

class ArticleRevisionRepository implements RepositoryInterface
{
    public function all(array $columns = ['*'])
    {
        $status = app('request')->get('status');
        $name = app('request')->get('name');
        $highlight = app('request')->get('highlight');
        $limit = app('request')->get('limit');
        $page = app('request')->get('page');

        $articleBuilder = ArticleRevision::isOpen()->filterStatus($status)->filterName($name)->filterHighlight($highlight);

        if (is_null($page)) {
            return $articleBuilder->get();
        }

        return $articleBuilder->paginate(
            $limit ?? 8,
            ['*'],
            'page',
            $page ?? 1
        );
    }

    public function count() : int
    {
        return ArticleRevision::count();
    }

    public function paginate(int $perPage = 15, $columns = ['*'])
    {
        return null;
    }

    public function create(array $data): Model
    {
        $article = ArticleRevision::create($data);

        return $article;
    }

    public function update(array $data, int $id)
    {
        $article = $this->find($id);

        $article->update($data);

        return $article->refresh();
    }

    public function updateBy(string $field, string $value, array $data)
    {
        $article = $this->findBy($field, $value);
        $article->update($data);

        return $article->refresh();
    }

    public function delete(int $id)
    {
        return null;
    }

    public function find(int $id, $columns = array('*'))
    {
        try {
            $article = ArticleRevision::findOrFail($id);
        }
        catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            throw new \App\Exceptions\ModelNotFoundException;
        }

        return $article;
    }

    public function findBy(string $field, string $value, $columns = ['*'])
    {
        try {
            $article = ArticleRevision::where($field, $value)->get();
        }
        catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            throw new \App\Exceptions\ModelNotFoundException;
        }

        return $article;
    }
}