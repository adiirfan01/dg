<?php

namespace App\Listeners\VideoComment;

use App\Events\Comment\VideoComment;
use App\Models\Video;


/**
 * Class: SendEmail
 *
 * Send email to all member after joining the tournament
 */
class SendEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  EntrylistAdded  $event
     * @return void
     */
    public function handle(VideoComment $event)
    {
        $videoId = $event->videoComment->videoId ?? null;
        $video = $this->getVideo($videoId);
        if($video){
            $user = $this->getUser($video->authorId);

            // TODO email user using data from
        }
    }
    public function getVideo($videoId){
        $video = Video::query()->where('id',$videoId)->first();
        return $video;
    }
    public function getUser($authorId){
        //TODO grab userdata from user Service
        return null;
    }
}
