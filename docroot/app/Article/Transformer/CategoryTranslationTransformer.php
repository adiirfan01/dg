<?php
/**
 * Created by PhpStorm.
 * User: salt
 * Date: 04/02/19
 * Time: 13:47
 */

namespace App\Article\Transformer;

use App\Models\CategoryTranslation;
use League\Fractal\TransformerAbstract;

class CategoryTranslationTransformer extends TransformerAbstract
{
    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(CategoryTranslation $translation)
    {
        return [
            'name' => $translation->name,
            'locale' => $translation->locale,
        ];
    }
}