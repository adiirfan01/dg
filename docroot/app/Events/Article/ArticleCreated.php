<?php

namespace App\Events\Article;

use App\Models\Article;

class ArticleCreated extends Event
{
    public $entrylist;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Article $article)
    {
        $this->article = $article;
    }
}
