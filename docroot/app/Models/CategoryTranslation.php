<?php
/**
 * Created by PhpStorm.
 * User: salt
 * Date: 08/11/18
 * Time: 11:48
 */
namespace App\Models;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;
use Cviebrock\EloquentSluggable\Sluggable;

/**
 * @property int $id
 * @property int $categoryId
 * @property string $name
 * @property string $locale
 */

class CategoryTranslation extends Model
{
    protected $table = 'category_translation';

    /**
     * @var array
     */
    protected $fillable = [
        'categoryId',
        'name',
        'locale'
    ];

    /**
     * Get the category
     */
    public function category()
    {
        return $this->belongsTo(category::class, 'categoryId');
    }

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

}