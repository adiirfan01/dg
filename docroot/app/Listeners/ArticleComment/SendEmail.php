<?php

namespace App\Listeners\ArticleComment;

use App\Events\Comment\ArticleComment;
use App\Models\Article;


/**
 * Class: SendEmail
 *
 * Send email to all member after joining the tournament
 */
class SendEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  EntrylistAdded  $event
     * @return void
     */
    public function handle(ArticleComment $event)
    {
        $articleId = $event->articleComment->articleId ?? null;
        $article = $this->getArticle($articleId);
        if($article){
            $user = $this->getUser($article->authorId);

            // TODO email user using data from
        }
    }
    public function getArticle($articleId){
        $video = Article::query()->where('id',$articleId)->first();
        return $video;
    }
    public function getUser($authorId){
        //TODO grab userdata from user Service
        return null;
    }
}
