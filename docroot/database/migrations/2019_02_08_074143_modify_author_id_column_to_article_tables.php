<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyAuthorIdColumnToArticleTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Db::statement('ALTER TABLE article CHANGE COLUMN `authorId` `authorId` varchar(255) NOT NULL;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Db::statement('ALTER TABLE article CHANGE COLUMN `authorId` `authorId` BIGINT(20) NOT NULL;');
    }
}
