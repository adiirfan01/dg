<?php
/**
 * Created by PhpStorm.
 * User: salt
 * Date: 04/02/19
 * Time: 13:20
 */

namespace App\Article\Transformer;

use App\Models\VideoComment;
use League\Fractal\TransformerAbstract;
use App\Helpers\GetDataUser;
use App\Models\VideoLikeComment;
use Carbon\Carbon;

class VideoCommentTransformer extends TransformerAbstract
{
    /**
     * List of resources default to include
     *
     * @var array
     */
    protected $defaultIncludes = [
        'reply'
    ];

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(VideoComment $comment)
    {
        return [
            'Id' =>  $comment->id,
            'videoId' =>  $comment->videoId,
            'user' =>  $this->getDataAuth0($comment->userId),
            'like' =>  $this->countLike($comment->id),
            'dislike' =>  $this->countDislike($comment->id),
            'comment' =>  $comment->comment,
            'createdDate' => $comment->createdDate,
            'dateNow' => Carbon::now(),
            'isLiked' => $this->checkLike($comment->id,1),
            'isDisliked' => $this->checkLike($comment->id,0),
        ];
    }
    /**
     * Count Like and Dislike
     */
    public function checkLike($id,$type){
        $userId = app('request')->input('userId');
        if(is_null($userId)){
            return 0;
        }
        $bookmarks = VideoLikeComment::where('commentId',$id)
            ->where('type',$type)
            ->where('userId',$userId)
            ->where('isDeleted',0)
            ->first();
        if(empty($bookmarks)){
            return 0;
        }
        return 1;
    }
    public function countLike($id){
        $total = VideoLikeComment::where('commentId',$id)
            ->where('type',1)
            ->where('isDeleted',0)
            ->count();
        return $total;
    }
    public function countDislike($id){
        $total = VideoLikeComment::where('commentId',$id)
            ->where('type',0)
            ->where('isDeleted',0)
            ->count();
        return $total;
    }
    public function getDataAuth0($userId)
    {
        $dataUser = new GetDataUser();
        return $dataUser->getMinimalProfile($userId);
    }

    /**
     * Include translation
     *
     * @return \League\Fractal\Resource\Collection
     */
    public function includeReply(VideoComment $comment)
    {
        $reply = $comment->replyComment;

        return $this->collection($reply, new VideoCommentReplyTransformer());
    }
}