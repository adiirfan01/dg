<?php
/**
 * Created by PhpStorm.
 * User: salt
 * Date: 04/02/19
 * Time: 12:14
 */

namespace App\Repositories\Video;

use App\Models\Revision;
use App\Models\Video;
use App\Repositories\RepositoryInterface;
use Illuminate\Database\Eloquent\Model;

class RevisionRepository implements RepositoryInterface
{
    public function all(array $columns = ['*'])
    {
       return null;
    }

    public function count() : int
    {
        return Revision::count();
    }

    public function paginate(int $perPage = 15, $columns = ['*'])
    {
        return null;
    }

    public function create(array $data): Model
    {
        $video = Video::findorfail($data['videoId']);
        if($video){
            $revision = Revision::create($data);
            $video->update([
                'status' => 3
            ]);
            return $video;
        }else{
            return null;
        }
    }
    public function addRevision(array $data,$id){
        $data['videoId'] = $id;
        $video = Video::findorfail($id);
        if($video){
            $notes = Revision::create($data);
            $video->update([
                'status' => 3
            ]);
        }
        return $video->refresh();
    }

    public function update(array $data, int $id)
    {
        $revision = $this->find($id);
        $revision->update($data);
        return $revision->refresh();
    }

    public function updateBy(string $field, string $value, array $data)
    {
        return null;
    }

    public function delete(int $id)
    {
        return null;
    }

    public function find(int $id, $columns = array('*'))
    {
        try {
            $video = Revision::findOrFail($id);
        }
        catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            throw new \App\Exceptions\ModelNotFoundException;
        }

        return $video;
    }

    public function findBy(string $field, string $value, $columns = ['*'])
    {
        try {
            $video = Revision::where($field, $value)->get();
            if(empty($video)){
                throw new \App\Exceptions\ModelNotFoundException;
            }
        }
        catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            throw new \App\Exceptions\ModelNotFoundException;
        }

        return $video;
    }

    public function related(int $id, $columns = array('*'))
    {
       return null;
    }
}