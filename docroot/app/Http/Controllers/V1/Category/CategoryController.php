<?php
/**
 * Created by PhpStorm.
 * User: salt
 * Date: 04/02/19
 * Time: 13:50
 */

namespace App\Http\Controllers\V1\Category;

use App\Http\Controllers\V1\Category;
use App\Http\Request\StoreCategoryRequest;
use Illuminate\Http\Request;

class CategoryController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $category = $this->repo->all();

        return $this->getSuccess('success', $category);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save(StoreCategoryRequest $request)
    {
        $category = $this->repo->create($request->all());

        return $this->getSuccess('success', $category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug, StoreCategoryRequest $request)
    {
        $request->merge([
            'slug'=> $slug
        ]);

        $category = $this->repo->updateBy('slug', $slug, $request->all());

        return $this->getSuccess('success', $category);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($slug)
    {
        $this->repo->delete($slug);

        return $this->getSuccess('deleted');
    }
}