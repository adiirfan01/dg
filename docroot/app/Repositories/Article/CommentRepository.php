<?php
/**
 * Created by PhpStorm.
 * User: salt
 * Date: 04/02/19
 * Time: 12:15
 */

namespace App\Repositories\Article;

use App\Models\Comment;
use App\Models\LikeComment;
use App\Repositories\RepositoryInterface;
use Illuminate\Database\Eloquent\Model;

class CommentRepository implements RepositoryInterface
{
    public function all(array $columns = ['*'])
    {
        /*$articleId = app('request')->get('articleId');

        if(!$articleId){
            return Comment::all();
        }

        return Comment::filterArticle($articleId);*/

        return null;

    }

    public function count() : int
    {
        return Comment::count();
    }

    public function paginate(int $perPage = 15, $columns = ['*'])
    {
        return null;
    }

    public function create(array $data): Model
    {
        $comment = Comment::create($data);

        return $comment;
    }

    public function update(array $data, int $id)
    {
        $comment = $this->find($id);

        $comment->update($data);

        return $comment->refresh();
    }

    public function updateBy(string $field, string $value, array $data)
    {
        $comment = $this->findBy($field, $value);
        $comment->update($data);

        return $comment->refresh();
    }

    public function delete(int $id)
    {
        return null;
    }

    public function find(int $id, $columns = array('*'))
    {
        try {
            $comment = Comment::findOrFail($id);
        }
        catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            throw new \App\Exceptions\ModelNotFoundException;
        }

        return $comment;
    }

    public function findBy(string $field, string $value, $columns = ['*'])
    {
        try {
            $comment = Comment::where($field, $value)->firstOrFail();
        }
        catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            throw new \App\Exceptions\ModelNotFoundException;
        }

        return $comment;
    }
    public function getBy(string $field, string $value, $columns = ['*'])
    {
        try {
            $comment = Comment::where($field, $value)->get();
        }
        catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            throw new \App\Exceptions\ModelNotFoundException;
        }

        return $comment;
    }
    public function like(array $data,$id,$type){
        $checkcomment = Comment::where('id',$id)->first();
        if(empty($checkcomment)){
            return null;
        }
        try {
            $comment = LikeComment::where('commentId', $id)
                ->where('userId',$data['userId'])
                ->firstOrFail();
        }
        catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            $comment = LikeComment::create([
                'userId' => $data['userId'],
                'commentId' => $id,
                'type' => $type,
                'isVideo' => 0
            ]);
            return $comment;
        }
       LikeComment::where('id',$comment->id)->update([
            'type' => $type,
            'isDeleted' => 0,
        ]);
        return $comment;
    }
    public function unlike(array $data,$id){
        $checkcomment = Comment::where('id',$id)->first();
        if(empty($checkcomment)){
            return null;
        }
        try {
            $comment = LikeComment::where('commentId', $id)
                ->where('userId',$data['userId'])
                ->firstOrFail();
        }
        catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return null;
        }
        LikeComment::where('id',$comment->id)->update([
            'isDeleted' => 1,
        ]);
        return $comment;
    }
}