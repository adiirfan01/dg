<?php
/**
 * Created by PhpStorm.
 * User: salt
 * Date: 04/02/19
 * Time: 14:44
 */

namespace App\Repositories\Video;

use App\Models\VideoCommentReply;
use App\Models\VideoComment;
use App\Repositories\RepositoryInterface;
use Illuminate\Database\Eloquent\Model;

class VideoReplyCommentRepository implements RepositoryInterface
{
    public function all(array $columns = ['*'])
    {
        $comment = app('request')->get('comment');
        $limit = app('request')->get('limit');
        $page = app('request')->get('page');
        $commentBuilder = CommentReply::Comments($comment);

        if(empty($comment)){
            return null;
        }
        if (is_null($page)) {
            return $commentBuilder->get();
        }

        return $commentBuilder->paginate(
            $limit ?? 5,
            ['*'],
            'page',
            $page ?? 1
        );
    }

    public function count() : int
    {
        return VideoCommentReply::count();
    }

    public function paginate(int $perPage = 15, $columns = ['*'])
    {
        return null;
    }

    public function create(array $data): Model
    {
        $check = VideoComment::find($data['commentId']);
        if ($check){
            $replyComment = VideoCommentReply::create($data);
            return $replyComment;
        }else{
            throw new \App\Exceptions\ModelNotFoundException;
        }

    }

    public function update(array $data, int $id)
    {
        $replyComment = $this->find($id);

        $replyComment->update($data);

        return $replyComment->refresh();
    }

    public function updateBy(string $field, string $value, array $data)
    {
        $replyComment = $this->findBy($field, $value);
        $replyComment->update($data);

        return $replyComment->refresh();
    }

    public function delete(int $id)
    {
        return null;
    }

    public function find(int $id, $columns = array('*'))
    {
        try {
            $replyComment = VideoCommentReply::findOrFail($id);
        }
        catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            throw new \App\Exceptions\ModelNotFoundException;
        }

        return $replyComment;
    }

    public function findBy(string $field, string $value, $columns = ['*'])
    {
        try {
            $replyComment = VideoCommentReply::where($field, $value)->firstOrFail();
        }
        catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            throw new \App\Exceptions\ModelNotFoundException;
        }

        return $replyComment;
    }
    public function related(int $id, $columns = ['*'])
    {
        // TODO: Implement related() method.
    }
}