<?php
/**
 * Created by PhpStorm.
 * User: salt
 * Date: 04/02/19
 * Time: 13:46
 */

namespace App\Article\Transformer;

use App\Models\Media;
use League\Fractal\TransformerAbstract;

class MediaTransformer extends TransformerAbstract
{
    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(Media $detail)
    {
        return [
            'uuid' => $detail->uuid,
            'url' => url('/').'/'.$detail->url,
        ];
    }
}