<?php
/**
 * Created by PhpStorm.
 * User: SALT
 * Date: 27/02/2019
 * Time: 17:07
 */

namespace App\Article\Transformer;

use App\Models\ArticleTranslation;
use League\Fractal\TransformerAbstract;

class ArticlePromotionTransformer extends TransformerAbstract
{
    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(ArticleTranslation $translation)
    {
        return [
            'title' => $translation->title,
            'slug' => $translation->slug,
            'excerpt' => $translation->excerpt,
            'content' => $translation->content,
            'locale' => $translation->locale,
            'articleId' => $translation->articleId,
        ];
    }

}