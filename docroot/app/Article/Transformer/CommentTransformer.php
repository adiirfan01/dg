<?php
/**
 * Created by PhpStorm.
 * User: salt
 * Date: 04/02/19
 * Time: 13:45
 */

namespace App\Article\Transformer;

use App\Models\Comment;
use Carbon\Carbon;
use League\Fractal\TransformerAbstract;
use App\Helpers\GetDataUser;
use App\Models\LikeComment;

class CommentTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $defaultIncludes = ['reply'];

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(Comment $comment)
    {
        return [
            'commentId' => $comment->id,
            'articleId' => $comment->articleId,
            'userId' => $this->getDataAuth0($comment->userId),
            'comment' => $comment->comment,
            'dateNow' => Carbon::now(),
            'createdDate' => $comment->createdDate,
            'updatedDate' => $comment->updatedDate,
            'like' => $this->countLike($comment->id),
            'dislike' => $this->countDislike($comment->id),
            'isLiked' => $this->checkLike($comment->id,1),
            'isDisliked' => $this->checkLike($comment->id,0),
        ];
    }
    /**
     *Count Like and Dislike
     */
    public function checkLike($id,$type){
        $userId = app('request')->input('userId');
        if(is_null($userId)){
            return 0;
        }
        $bookmarks = LikeComment::where('commentId',$id)
            ->where('type',$type)
            ->where('userId',$userId)
            ->where('isDeleted',0)
            ->first();
        if(empty($bookmarks)){
            return 0;
        }
        return 1;
    }
    public function countLike($id){
        $total = LikeComment::where('commentId',$id)
            ->where('type',1)
            ->where('isDeleted',0)
            ->count();
        return $total;
    }
    public function countDislike($id){
        $total = LikeComment::where('commentId',$id)
            ->where('type',0)
            ->where('isDeleted',0)
            ->count();
        return $total;
    }
    /**
     * Include Reply
     *
     * @return \League\Fractal\Resource\Collection
     */
    public function includeReply(Comment $comment)
    {
        $reply = $comment->replyComment;

        return $this->collection($reply, new CommentReplyTransformer());
    }

    public function getDataAuth0($userId)
    {
        $dataUser = new GetDataUser();
        return $dataUser->getMinimalProfile($userId);
    }
}