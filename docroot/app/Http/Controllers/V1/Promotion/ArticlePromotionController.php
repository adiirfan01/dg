<?php

namespace App\Http\Controllers\V1\Promotion;

use Illuminate\Http\Request;
use App\Http\Controllers\V1\Promotion\BaseController;

class ArticlePromotionController extends BaseController
{
    public function index()
    {
        $promotion = $this->repo->all();

        return $this->promotionMultiDataTransformer($this->getSuccess('success', $promotion));
    }

}