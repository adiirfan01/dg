<?php
/**
 * Created by PhpStorm.
 * User: salt
 * Date: 04/02/19
 * Time: 13:46
 */

namespace App\Article\Transformer;

use App\Models\Revision;
use League\Fractal\TransformerAbstract;

class RevisionTransformer extends TransformerAbstract
{
    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(Revision $revision)
    {
        return [
            'id' => $revision->id,
            'videoId' => $revision->videoId,
            'content' => $revision->content,
            'comment' => $revision->comment,
            'status' => $revision->status,
        ];
    }
}