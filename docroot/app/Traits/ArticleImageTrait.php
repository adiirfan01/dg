<?php
/**
 * Created by PhpStorm.
 * User: salt
 * Date: 01/02/19
 * Time: 19:44
 */

namespace App\Traits;

use App\Article\ImageSave;

trait ArticleImageTrait
{
    public function setImageAttribute($value)
    {
        if (is_string($value)) {
            return $this->attributes['image'] = $value;
        }

        $filename = ImageSave::save($value);

        $this->attributes['image'] = $filename;
    }


    public function getImageUrlAttribute()
    {
        if (!array_key_exists('image', $this->attributes)) return null;
        $image = $this->attributes['image'];
        if($image == null) return null;
        if(strpos($image, "https") !== false || strpos($image, "http") !== false){
            return $image;
        }
        return url() . '/' . $image;
    }

}