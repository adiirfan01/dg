<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Models\VideoDetail::class, function (Faker\Generator $faker) {
    $title =  $faker->sentence($nbWords = 4, $variableNbWords = true);
    return [
        'videoId' => '1',
        'title' => $title,
        'slug' => str_slug($title),
        'desc' => $faker->sentence($nbWords = 40, $variableNbWords = true),
        'locale' => 'id'
    ];
});
