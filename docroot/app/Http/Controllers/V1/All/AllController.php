<?php

namespace App\Http\Controllers\V1\All;

use Illuminate\Http\Request;


class AllController extends BaseController
{
    public function index()
    {
        $article = $this->repo->all();
        return $this->getAll('success', $article);
    }
    public function latest()
    {
        $article = $this->repo->all();

        return $this->getAll('success', $article,200,true);
    }
}
