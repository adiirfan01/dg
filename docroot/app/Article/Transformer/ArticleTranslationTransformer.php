<?php
/**
 * Created by PhpStorm.
 * User: salt
 * Date: 04/02/19
 * Time: 13:46
 */

namespace App\Article\Transformer;

use App\Models\ArticleTranslation;
use League\Fractal\TransformerAbstract;
use App\Helpers\attachmentConverter;

class ArticleTranslationTransformer extends TransformerAbstract
{
    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(ArticleTranslation $translation)
    {
        return [
            'title' => $translation->title,
            'slug' => $translation->slug,
            'excerpt' => $translation->excerpt,
            'content' => $this->translate($translation->content,$translation->id),
            'locale' => $translation->locale,
            'articleId' => $translation->articleId,
        ];
    }
    public function translate($detail,$id)
    {
        $paragraph = new attachmentConverter();
        return $paragraph->getAttachment($detail,$id);
    }

}