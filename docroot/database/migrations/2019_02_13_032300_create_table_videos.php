<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableVideos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('video', function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->string('authorId',255);
            $table->bigInteger('categoryId');
            $table->string('videoUrl', 255);
            $table->tinyInteger('inEditor')->default(0);
            $table->string('authorName', 255);
            $table->dateTime('publishedDate')->nullable(true);
            $table->tinyInteger('status')->default(0);
            $table->tinyInteger('highlight')->default(0);
            $table->bigInteger('totalViews')->default(0);
            $table->string('image', 255);
            $table->dateTime('createdDate');
            $table->string('createdBy', 255);
            $table->dateTime('updatedDate')->nullable(true);
            $table->string('updatedBy', 255)->nullable(true);
            $table->tinyInteger('isDeleted')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('video');
    }
}
