<?php

namespace App\Http\Request;

use Illuminate\Validation\Rule;

class StoreUpdateVideoRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'userId' => 'required',
            'title' => 'required',
            'desc' => 'required',
            'videoUrl' => 'required',
        ];
    }


    protected function getValidatorInstance()
    {
        $this->getInputSource()->replace($this->modifyData());
        $validator = parent::getValidatorInstance();

        return $validator;
    }

    protected function modifyData()
    {
        $data = $this->validationData();

        if(array_key_exists('userId', $data)){
            $data['createdBy'] = $data['userId'];
            $data['authorId'] = $data['userId'];
        }else{
            $data['createdBy'] = "sms|5c6e37069e6bbdc8bc73fa9c";
            $data['authorId'] = "sms|5c6e37069e6bbdc8bc73fa9c";
        }
        return $data;
    }
}
