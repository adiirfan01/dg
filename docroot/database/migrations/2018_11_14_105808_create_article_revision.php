<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticleRevision extends Migration
{
    private const ARTICLE_REVISION_TABLE = "article_revision";
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::create(self::ARTICLE_REVISION_TABLE, function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->string('uuid', 255);
            $table->bigInteger('articleId');
            $table->integer('version');
            $table->string('locale');
            $table->string('title', 255);
            $table->json('metadata');
            $table->dateTime('createdDate');
            $table->string('createdBy', 255);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(self::ARTICLE_REVISION_TABLE);
    }
}
