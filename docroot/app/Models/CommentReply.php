<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;

/**
 * @property int $id
 * @property int $commentId
 * @property int $userId
 * @property string $comment
 * @property \Carbon\Carbon $createdDate
 * @property string $createdBy
 * @property \Carbon\Carbon $updatedDate
 * @property string $updatedBy
 * @property boolean $isDeleted
 */
class CommentReply extends Model
{
    use SoftDeletes;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'article_reply_comment';

    const CREATED_AT = 'createdDate';
    const UPDATED_AT = 'updatedDate';
    const DELETED_AT = 'deletedDate';

    /**
     * @var array
     */
    protected $fillable = [
        'commentId',
        'userId',
        'comment',
        'isDeleted',
        'createdDate',
        'updatedDate',
        'deletedDate',
        'createdBy',
        'updatedBy'
    ];

    /**
     * Get the Article Comment
     */
    public function comment()
    {
        return $this->belongsTo(Comment::class, 'commentId');
    }

}
