<?php

namespace App\Article;
use Illuminate\Support\Facades\File;

class ImageSave
{

    public static function save($image, $type = null)
    {
        if (is_null($image)) return null;

        if (is_string($image)) return $image;

        $fileName   = rand() . time() . '.' . $image->getClientOriginalExtension();
        $destinationPath = 'upload/image/';

        $image->move(public_path().'/'.$destinationPath, $fileName);

        return $destinationPath . $fileName;
    }
    public static function delete($image)
    {
        $image_path = public_path().'/'.$image;
        if (File::exists($image_path)) {
            $delete = unlink($image_path);
            if($delete){
                return true;
            }
            return false;
        }
        return false;
    }
}
