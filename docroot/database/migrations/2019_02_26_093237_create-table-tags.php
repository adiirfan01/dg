<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTags extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tags', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug');
            $table->string('createdBy', 255);
            $table->dateTime('createdDate')->nullable(true);
            $table->dateTime('updatedDate')->nullable(true);
            $table->string('updatedBy', 255)->nullable(true);
            $table->tinyInteger('isDeleted')->default(0);
            $table->dateTime('deletedDate')->nullable(true);
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tags');
    }
}
