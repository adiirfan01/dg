<?php
/**
 * Created by PhpStorm.
 * User: salt
 * Date: 04/02/19
 * Time: 13:47
 */

namespace App\Article\Transformer;


use League\Fractal\TransformerAbstract;

class NullTransformer extends TransformerAbstract
{
    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform()
    {
        return [
            'name' => null,
            'locale' => null,
        ];
    }
}