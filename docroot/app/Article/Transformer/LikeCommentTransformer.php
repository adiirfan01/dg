<?php
/**
 * Created by PhpStorm.
 * User: salt
 * Date: 04/02/19
 * Time: 13:45
 */

namespace App\Article\Transformer;

use App\Models\LikeComment;
use League\Fractal\TransformerAbstract;

class LikeCommentTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $defaultIncludes = [];

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(LikeComment $comment)
    {
        return [
            'like' => $this->countLike($comment->commentId),
            'dislike' => $this->countDislike($comment->commentId),
        ];
    }
    public function countLike($id){
        $total = LikeComment::where('commentId',$id)
            ->where('type',1)
            ->where('isDeleted',0)
            ->count();
        return $total;
    }
    public function countDislike($id){
        $total = LikeComment::where('commentId',$id)
            ->where('type',0)
            ->where('isDeleted',0)
            ->count();
        return $total;
    }
}