<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;

/**
 * @property int $id
 * @property int $commentId
 * @property int $userId
 * @property string $comment
 * @property \Carbon\Carbon $createdDate
 * @property string $createdBy
 * @property \Carbon\Carbon $updatedDate
 * @property string $updatedBy
 * @property boolean $isDeleted
 */
class VideoCommentReply extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'video_comment_reply';

    const CREATED_AT = 'createdDate';
    const UPDATED_AT = 'updatedDate';

    /**
     * @var array
     */
    protected $fillable = [
        'commentId',
        'userId',
        'comment',
        'like',
        'dislike',
        'isDeleted',
        'createdDate',
        'updatedDate',
        'createdBy',
        'updatedBy'
    ];

    /**
     * Get the Video Comment
     */
    public function comment()
    {
        return $this->belongsTo(Comment::class, 'commentId');
    }
    public function scopeComments($query, $Id)
    {
        if (is_null($Id)) return $query;

        return $query->where('commentId', '=', $Id);
    }

}
