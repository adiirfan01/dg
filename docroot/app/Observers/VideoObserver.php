<?php
namespace App\Observer;

use App\Events\Video\VideoCreated;
use App\Models\Video;
use App\Events\Video\VideoPublished;
use App\Events\Video\VideoUpdated;
use App\Events\Video\VideoRevision;
use App\Events\Video\VideoRejected;

class VideoObserver
{
    public function updating(Video $video){
        if($video->isDirty('status') && $video->status == 2){
            //video need review
            event(new VideoUpdated($video));
        }
        if($video->isDirty('status') && $video->status == 3){
            //video harus direvisi
            event(new VideoRevision($video));
        }
        if($video->isDirty('status') && $video->status == 4){
            //video rejected
            event(new VideoRejected($video));
        }
        if($video->isDirty('status') && $video->status == 1){
            //video published
            event(new VideoPublished($video));
        }
    }
    public function creating(Video $video){
        if($video->isDirty('status') && $video->status == 2){
            //video created
            event(new VideoCreated($video));
        }
    }
}