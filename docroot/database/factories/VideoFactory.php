<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Models\Video::class, function (Faker\Generator $faker) {
    return [
        'authorId' => '1',
        'categoryId' => '1',
        'videoUrl' => 'https://youtu.be/CWeySpqDNYA',
        'inEditor' => '0',
        'publishedDate'=> $faker->date('Y-m-d','now'),
        'status' => '1',
        'highlight' => '0',
        'totalViews' => '0',
        'image' => $faker->imageUrl($width = 640, $height = 480),
        'createdDate' => $faker->date('Y-m-d','now'),
        'createdBy' => '1',
        'isDeleted' => '0',
    ];
});
