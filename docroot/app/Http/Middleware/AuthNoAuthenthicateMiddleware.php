<?php
/**
 * Created by PhpStorm.
 * User: Salt
 * Date: 22/12/2018
 * Time: 22:07
 */

namespace App\Http\Middleware;

use Closure;
use Auth0\SDK\JWTVerifier;
use Illuminate\Http\JsonResponse;

class AuthNoAuthenthicateMiddleware
{
    /**
     * Run the request filter.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     * @throws \Auth0\SDK\Exception\CoreException
     */
    public function handle($request, Closure $next)
    {
        $responseCode = 401;

        if($request->hasHeader('Authorization')){
            $authorization = $request->header('Authorization');
        }elseif ($request->hasHeader('authorization')){
            $authorization = $request->header('authorization');
        }else{
            $request->request->add(['userId' => null]);
            return $next($request);
        }

        $token = null;
        $authorizationHeader = str_replace('bearer ', '', $authorization);
        $token = str_replace('Bearer ', '', $authorizationHeader);

        if($authorization == null || $token == null) {
            return new JsonResponse([
                'status' => [
                    'code'    => 1,
                    'message' => "No token provided",
                    'errorMessage' => 'Error, Invalid Token'
                ]
            ], $responseCode);
        }

        try {
            $verifier = new JWTVerifier([
                'supported_algs' => [getenv('AUTH0_SUPPORTED_ALGS')],
                'valid_audiences' => [getenv('AUTH0_IDENTIFIER')],
                'authorized_iss' => [getenv('AUTH0_AUTHORIZED_IIS')]
            ]);

            $decoded = $verifier->verifyAndDecode($token);
        }
        catch(\Auth0\SDK\Exception\CoreException $e) {
            $responseCode = ($e->getCode() != null) ? $e->getCode() : 401;

            return new JsonResponse([
                'status' => [
                    'code'    => 1,
                    'message' => "Invalid Token",
                    'errorMessage' => 'Error, ' . $e->getMessage()
                ]
            ], $responseCode);
        };

        $request->merge([
            'userId' => $decoded->sub,
            'authorId' => $decoded->sub
        ]);

        return $next($request);
    }

}
