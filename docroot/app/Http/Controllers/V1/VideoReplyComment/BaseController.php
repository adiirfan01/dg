<?php
/**
 * Created by PhpStorm.
 * User: salt
 * Date: 04/02/19
 * Time: 14:42
 */

namespace App\Http\Controllers\V1\VideoReplyComment;

use App\Http\Controllers\V1\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class BaseController extends Controller
{
    public function getMyReplyComment(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->getErrorValidation($validator->getMessageBag()->first());
        }

        $replyComment = $this->repo->findBy('userId', $request->userId);

        return $this->getSuccess('success', $replyComment);
    }

}