<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticleNotes extends Migration
{
    private const ARTICLE_NOTES_TABLE = "article_notes";
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::create(self::ARTICLE_NOTES_TABLE, function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->bigInteger('articleId');
            $table->bigInteger('userId');
            $table->text('notes');
            $table->dateTime('createdDate');
            $table->string('createdBy', 255);
            $table->dateTime('updatedDate')->nullable(true);
            $table->string('updatedBy', 255)->nullable(true);
            $table->tinyInteger('isDeleted')->default(0);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(self::ARTICLE_NOTES_TABLE);
    }
}
