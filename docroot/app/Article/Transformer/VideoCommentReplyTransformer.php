<?php
/**
 * Created by PhpStorm.
 * User: salt
 * Date: 04/02/19
 * Time: 13:47
 */

namespace App\Article\Transformer;

use App\Models\VideoCommentReply;
use League\Fractal\TransformerAbstract;
use App\Helpers\GetDataUser;
use Carbon\Carbon;

class VideoCommentReplyTransformer extends TransformerAbstract
{
    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(VideoCommentReply $commentReply)
    {
        return [
            'Id' => $commentReply->id,
            'commentId' => $commentReply->commentId,
            'user' => $this->getDataAuth0($commentReply->userId),
            'comment' => $commentReply->comment,
            'dateNow' => Carbon::now(),
            'createdDate' => $commentReply->createdDate,
            'updatedDate' => $commentReply->updatedDate
        ];
    }

    public function getDataAuth0($userId)
    {
        $dataUser = new GetDataUser();
        return $dataUser->getMinimalProfile($userId);
    }
}