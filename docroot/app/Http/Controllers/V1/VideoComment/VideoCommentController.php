<?php
/**
 * Created by PhpStorm.
 * User: salt
 * Date: 04/02/19
 * Time: 13:51
 */

namespace App\Http\Controllers\V1\VideoComment;

use App\Http\Controllers\V1\VideoComment;
use App\Http\Request\StoreCommentRequest;
use Illuminate\Http\Request;

class VideoCommentController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($videoId)
    {

        $comment = $this->repo->findBy('videoId',$videoId);

        return $this->getSuccess('success', $comment);
    }
    public function single($id)
    {
        $video = $this->repo->find($id);

        return $this->getSuccess('success', $video);
    }
    public function update(int $id,StoreCommentRequest $request)
    {
        $video = $this->repo->update($request->all(), $id);

        return $this->getSuccess('success', $video);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save($videoId,StoreCommentRequest $request)
    {
        $request->merge([
            'videoId' => $videoId,
        ]);
        $comment = $this->repo->create($request->all());

        return $this->getSuccess('success', $comment);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $comment = $this->repo->update($request->all(), $id);

        return $this->getSuccess('success', $comment);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $comment = $this->repo->delete($id);

        return $this->getSuccess('success', $comment);
    }
    public function like($id,Request $request)
    {
        $comment = $this->repo->like($request->all(), $id,1);
        return $this->getSuccess('success', $comment);
    }
    public function dislike($id,Request $request)
    {
        $comment = $this->repo->like($request->all(), $id,0);
        return $this->getSuccess('success', $comment);
    }
    public function unlike($id,Request $request)
    {
        $comment = $this->repo->unlike($request->all(), $id);
        return $this->getSuccess('success', $comment);
    }

}