<?php
/**
 * Created by PhpStorm.
 * User: salt
 * Date: 31/01/19
 * Time: 16:31
 */
namespace App\Providers;

use App\Http\Controllers\V1\Tags\{TagsController, BaseController as BaseTags};
use Illuminate\Support\ServiceProvider;
use App\Repositories\RepositoryInterface;
use App\Repositories\Tags\TagsRepository;

class TagsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {


    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->when(TagsController::class)
            ->needs(RepositoryInterface::class)
            ->give(function() {
                return new TagsRepository();
            });

        $this->app->when(BaseTags::class)
            ->needs(RepositoryInterface::class)
            ->give(function() {
                return new TagsRepository();
            });
    }
}