<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 21/03/2019
 * Time: 18:00
 */
use App\Models\Comment;
use App\Events\Comment\ArticleComment as EventComment;

class ArticleCommentObserver
{
    public function creating(Comment $comment){
        event(new EventComment($comment));
    }
}