<?php
/**
 * Created by PhpStorm.
 * User: salt
 * Date: 04/02/19
 * Time: 12:14
 */

namespace App\Repositories\Video;
use App\Models\VideoDetail;
use App\Repositories\RepositoryInterface;
use Illuminate\Database\Eloquent\Model;

class DetailRepository implements RepositoryInterface
{
    public function all(array $columns = ['*'])
    {
        return null;
    }

    public function count() : int
    {
        return VideoDetail::count();
    }

    public function paginate(int $perPage = 15, $columns = ['*'])
    {
        return null;
    }

    public function create(array $data): Model
    {
        $video = VideoDetail::create($data);

        return $video;
    }

    public function update(array $data, int $id)
    {
        $video = $this->find($id);
        $video->update($data);

        return $video->refresh();
    }

    public function updateBy(string $field, string $value, array $data)
    {
        $video = $this->findBy($field, $value);
        $video->update($data);

        return $video->refresh();
    }

    public function delete(int $id)
    {
        return null;
    }

    public function find(int $id, $columns = array('*'))
    {
        try {
            $video = VideoDetail::findOrFail($id);
        }
        catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            throw new \App\Exceptions\ModelNotFoundException;
        }

        return $video;
    }

    public function findBy(string $field, string $value, $columns = ['*'])
    {
        try {
            $video = VideoDetail::where($field, $value)->firstOrFail();
        }
        catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            throw new \App\Exceptions\ModelNotFoundException;
        }

        return $video;
    }
    public function related(int $id, $columns = ['*'])
    {
        // TODO: Implement related() method.
    }
}