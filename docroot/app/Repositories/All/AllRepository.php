<?php
/**
 * Created by PhpStorm.
 * User: salt
 * Date: 04/02/19
 * Time: 12:14
 */

namespace App\Repositories\All;

use App\Models\Video;
use App\Models\VideoDetail;
use App\Models\Article;
use App\Models\Category;
use App\Models\Bookmarks;
use App\Models\ArticleTranslation;
use App\Repositories\RepositoryInterface;
use Illuminate\Database\Eloquent\Model;
use \Cviebrock\EloquentSluggable\Services\SlugService;
use phpDocumentor\Reflection\Types\Integer;

class AllRepository implements RepositoryInterface
{
    public function all(array $columns = ['*']){
        $limit = app('request')->get('limit');
        if((empty($limit))){
            $video = $this->allVideo($columns);
            if(count($video) < 3){
                $article = $this->allArticle($columns,null,(11 - count($video)));
            }else{
                $article = $this->allArticle($columns);
                if(count($article) < 8) {
                    $video = $this->allVideo($columns,null,(11 - count($article)));
                }
            }
            $data['article'] = $article;
            $data['video'] = $video;
            return $data;
        }
        $limitarticle = ceil($limit / 2);
        $limitvideo  = $limit - $limitarticle;
        $article = $this->allArticle($columns,null,$limitarticle);
        $video = $video = $this->allVideo($columns,null,$limitvideo);
        $data['article'] = $article;
        $data['video'] = $video;
        return $data;
    }
    public function allVideo(array $columns,array $videoid=null,$limit = 3)
    {
        $status = 'published';
        $categoryId = null;
        $search = app('request')->get('search');
        $category = app('request')->get('category');
        if(!empty($category)){
            $categoryId = $this->getCategoryId($category);
        }
        $user = app('request')->get('user');
        $habit = app('request')->get('habit');
        $highlight = app('request')->get('highlight');
        $page = app('request')->get('page');
        $userpref = [];
        if(empty($videoid)){
            $locale = app('request')->input('locale');
            $videoDetail = VideoDetail::where('locale', $locale)->get();
            $videoid = array_map(function ($object) { return $object->videoId; }, json_decode($videoDetail));
        }
        if(!empty($search)){
            $videoDetail = VideoDetail::whereIn('videoId',$videoid)->filterSearch($search)->get();
            $videoid = array_map(function ($object) { return $object->videoId; }, json_decode($videoDetail));
        }
        $videoBuilder = Video::filterStatus($status)
            ->filterHighlight($highlight)
            ->filterCategory($categoryId)
            ->whereIn('id', $videoid);
        if(empty($user)){
            $videoBuilder = $videoBuilder->UserPref($userpref);
        }
        if (is_null($page)) {
            return $videoBuilder->get();
        }


        return $videoBuilder->orderBy('id', 'DESC')->paginate(
            $limit ?? 3,
            ['*'],
            'page',
            $page ?? 1
        );
    }
    public function allArticle(array $columns,array $articleid=null,$limit = 8)
    {
        $status = 'published';
        $categoryId = null;
        $search = app('request')->get('search');
        $category = app('request')->get('category');
        if(!empty($category)){
            $categoryId = $this->getCategoryId($category);
        }
        $highlight = app('request')->get('highlight');
        $habit = app('request')->get('habit');
        $page = app('request')->get('page');
        $locale = app('request')->input('locale');
        if(empty($articleid)) {
            $articleTrans = ArticleTranslation::where('locale', $locale)->get();
            $articleid = array_map(function ($object) {
                return $object->articleId;
            }, json_decode($articleTrans));
        }
        if(!empty($search)){
            $articleTrans = ArticleTranslation::whereIn('articleId',$articleid)->filterSearch($search)->get();
            $articleid = array_map(function ($object) { return $object->articleId; }, json_decode($articleTrans));
        }
        $articleBuilder = Article::filterStatus($status)
            ->filterHighlight($highlight)
            ->filterCategory($categoryId)
            ->whereIn('id', $articleid);
        if($habit == 1){
            $limit = 2;
            $page=1;
            $articleBuilder = $articleBuilder->inRandomOrder();
        }
        if (is_null($page)) {
            return $articleBuilder->get();
        }

        return $articleBuilder->orderBy('id', 'DESC')->paginate(
            $limit ?? 8,
            ['*'],
            'page',
            $page ?? 1
        );
    }
    public function bookmarks(){
        $limit = app('request')->get('limit');
        if(empty($limit)){
            $limit = 11;
        }
        $limitarticle = ceil($limit / 2);
        $limitvideo  = $limit - $limitarticle;
        $user = app('request')->get('userId');
        $getarticleid = Bookmarks::where('userId',$user)
            ->where('type','1')
            ->where('isDeleted',0)
            ->groupBy('articleId')
            ->pluck('articleId')->toArray();

        $getvideoid = Bookmarks::where('userId',$user)
            ->where('type','2')
            ->where('isDeleted',0)
            ->groupBy('articleId')
            ->pluck('articleId')->toArray();
        if(count($getvideoid) < 1){
            $video = [];
        }else{
            $video = $this->allVideo(['*'],$getvideoid,$limitvideo);
        }
        if(count($getarticleid) < 1){
            $article = [];
        }else{
            $article = $this->allArticle(['*'],$getarticleid,$limitarticle);
        }
        $data['article'] = $article;
        $data['video'] = $video;
        return $data;
    }

    public function count() : int
    {
        return Video::count();
    }

    public function paginate(int $perPage = 15, $columns = ['*'])
    {
        return null;
    }

    public function create(array $data): Model
    {
        return null;
    }

    public function update(array $data, int $id)
    {
        return null;
    }

    public function updateBy(string $field, string $value, array $data)
    {
        return null;
    }

    public function delete(int $id)
    {
        return null;
    }

    public function find(int $id, $columns = array('*'))
    {
        return null;
    }

    public function findBy(string $field, string $value, $columns = ['*'])
    {
        return null;
    }
    public function getCategoryId($slug){
        $getCatFromSlug = new Category();
        $getIdFromSlug = $getCatFromSlug->where('slug', $slug)->first();
        if(!$getIdFromSlug){
            return 0;
        }
        return $getIdFromSlug->id;
    }
}