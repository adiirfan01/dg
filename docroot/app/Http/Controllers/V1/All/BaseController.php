<?php

namespace App\Http\Controllers\V1\All;

use Illuminate\Http\Request;
use App\Http\Controllers\V1\Controller;

class BaseController extends Controller
{
    public function getMyBookmarks()
    {
        $bookmarks = $this->repo->bookmarks();
        if(!$bookmarks){
            return $this->getSuccessSimple('Empty',null);
        }
        return $this->getAll('success', $bookmarks);
    }
}
