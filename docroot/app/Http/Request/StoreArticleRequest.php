<?php

namespace App\Http\Request;
use App\Models\Category;
use App\Models\Tags;

class StoreArticleRequest extends FormRequest
{
    protected function validationData()
    {
        $data = $this->merge($this->all());
        return $data->request->all();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'userId' => 'required',
            'title' => 'required',
            'excerpt' => 'required',
            'content' => 'required',
            'tags' => 'required',
        ];
    }


    protected function getValidatorInstance()
    {
        $this->getInputSource()->replace($this->modifyData());
        $validator = parent::getValidatorInstance();

        return $validator;
    }

    protected function modifyData()
    {
        $data = $this->validationData();

        if(array_key_exists('userId', $data)){
            $data['createdBy'] = $data['userId'];
            $data['authorId'] = $data['userId'];
        }else{
            $data['createdBy'] = "1";
            $data['authorId'] = "1";
        }
        if(array_key_exists('categoryId', $data)){
            try {
                $category = Category::findOrFail($data['categoryId']);
            }
            catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
                throw new \App\Exceptions\CategoryNotFoundException();
            }
        }

        if(array_key_exists('tags',$data)){
            $tags = $data['tags'];
            $tags = substr($tags, 1);
            $tags = substr($tags, 0, -1);
            $tags = explode(",", $tags);
            for ($i = 0; $i < count($tags); $i++) {
                try {
                    Tags::findOrFail($tags[$i]);
                }catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
                    throw new \App\Exceptions\TagsNotFoundException();
                }
            }
        }
        return $data;
    }
}
