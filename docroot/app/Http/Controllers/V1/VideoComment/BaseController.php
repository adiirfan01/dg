<?php
/**
 * Created by PhpStorm.
 * User: salt
 * Date: 04/02/19
 * Time: 13:50
 */

namespace App\Http\Controllers\V1\VideoComment;

use App\Http\Controllers\V1\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class BaseController extends Controller
{
    public function getMyComment(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->getErrorValidation($validator->getMessageBag()->first());
        }

        $comment = $this->repo->findBy('userId', $request->userId);

        return $this->getSuccess('success', $comment);
    }
}