<?php

namespace App\Models;

use App\Traits\ArticleImageTrait;
use App\Traits\UuidTrait;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


/**
 * @property int $id
 * @property int $articleId
 * @property int $version
 * @property string $locale
 * @property string $title
 * @property string $metadata
 * @property \Carbon\Carbon $createdDate
 * @property string $createdBy
 */
class ArticleRevision extends Model
{
    use UuidTrait;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'article_revision';

    const CREATED_AT = 'createdDate';
    const UPDATED_AT = 'updatedDate';

    /**
     * @var array
     */
    protected $fillable = [
        'uuid',
        'articleId',
        'version',
        'locale',
        'metadata',
        'title',
        'createdDate',
        'createdBy',
    ];

    /**
     * Get the Article
     */
    public function article()
    {
        return $this->belongsTo(Article::class, 'articleId');
    }

}
