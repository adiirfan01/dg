<?php
/**
 * Created by PhpStorm.
 * User: salt
 * Date: 04/02/19
 * Time: 12:15
 */

namespace App\Repositories\Category;

use App\Models\CategoryTranslation;
use App\Repositories\RepositoryInterface;
use Illuminate\Database\Eloquent\Model;

class CategoryTranslationRepository implements RepositoryInterface
{
    public function all(array $columns = ['*'])
    {
        return null;
    }

    public function count() : int
    {
        return CategoryTranslation::count();
    }

    public function paginate(int $perPage = 15, $columns = ['*'])
    {
        return null;
    }

    public function create(array $data): Model
    {
        $category = CategoryTranslation::create($data);

        return $category;
    }

    public function update(array $data, int $id)
    {
        $category = $this->find($id);

        $category->update($data);

        return $category->refresh();
    }

    public function updateBy(string $field, string $value, array $data)
    {
        $category = $this->findBy($field, $value);
        $category->update($data);

        return $category->refresh();
    }

    public function delete(int $id)
    {
        return null;
    }

    public function find(int $id, $columns = array('*'))
    {
        try {
            $category = CategoryTranslation::findOrFail($id);
        }
        catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            throw new \App\Exceptions\ModelNotFoundException;
        }

        return $category;
    }

    public function findBy(string $field, string $value, $columns = ['*'])
    {
        try {
            $category = CategoryTranslation::where($field, $value)->get();
        }
        catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            throw new \App\Exceptions\ModelNotFoundException;
        }

        return $category;
    }
}