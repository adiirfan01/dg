<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 21/03/2019
 * Time: 18:00
 */
use App\Models\VideoComment;
use App\Events\Comment\VideoComment as EventComment;

class VideoCommentObserver
{
    public function creating(VideoComment $videoComment){
        event(new EventComment($videoComment));
    }
}