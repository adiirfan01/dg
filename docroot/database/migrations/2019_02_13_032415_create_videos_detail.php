<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideosDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('video_detail', function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->bigInteger('videoId');
            $table->string('title', 255);
            $table->string('slug', 255);
            $table->string('excerpt', 255);
            $table->text('desc');
            $table->string('metaTitle', 255)->nullable(true);
            $table->string('metaDescription', 255)->nullable(true);
            $table->string('metaKeyword', 255)->nullable(true);
            $table->string('metaFbTitle', 255)->nullable(true);
            $table->string('metaFbDescription', 255)->nullable(true);
            $table->string('metaFbImage', 255)->nullable(true);
            $table->string('metaTwitterTitle', 255)->nullable(true);
            $table->string('metaTwitterDescription', 255)->nullable(true);
            $table->string('metaTwitterImage', 255)->nullable(true);
            $table->string('metaRobotsIndex', 255)->nullable(true);
            $table->string('metaRobotsAdvance', 255)->nullable(true);
            $table->string('metaRobotsFollow', 255)->nullable(true);
            $table->string('metaCanonicalUrl', 255)->nullable(true);
            $table->string('locale', 255);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('video_detail');
    }
}
