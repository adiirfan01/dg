<?php

namespace App\Http\Request;

use App\Http\Request\FormRequest;
use Illuminate\Validation\Rule;

class StoreUpdateArticleRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'title' => 'required',
            'excerpt' => 'required',
            'content' => 'required',
            'userId' => 'required',
            'tags' => 'required',
        ];
    }


    protected function getValidatorInstance()
    {
        $this->getInputSource()->replace($this->modifyData());
        $validator = parent::getValidatorInstance();

        return $validator;
    }

    protected function modifyData()
    {
        $data = $this->validationData();

        if(array_key_exists('userId', $data)){
            $data['updatedBy'] = $data['userId'];
        }else{
            $data['updatedBy'] = "SYSTEM";
        }

        return $data;
    }
}
