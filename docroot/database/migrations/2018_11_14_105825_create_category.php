<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategory extends Migration
{
    private const CATEGORY_ARTICLE_TABLE   = 'category';
    private const CATEGORY_TRANSlATION_TABLE = "category_translation";
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {

        Schema::create(self::CATEGORY_ARTICLE_TABLE, function (Blueprint $table) {
            $table->biginteger('id', true);
            $table->string('slug', 255);
            $table->dateTime('createdDate');
            $table->string('createdBy', 255);
            $table->dateTime('updatedDate')->nullable(true);
            $table->string('updatedBy', 255)->nullable(true);
            $table->tinyInteger('isDeleted')->default(0);
        });
        Schema::create(self::CATEGORY_TRANSlATION_TABLE, function (Blueprint $table) {
            $table->biginteger('id', true);
            $table->biginteger('categoryId');
            $table->string('name');
            $table->string('locale')->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(self::CATEGORY_ARTICLE_TABLE);
        Schema::dropIfExists(self::CATEGORY_TRANSlATION_TABLE);
    }
}
