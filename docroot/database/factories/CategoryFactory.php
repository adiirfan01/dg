<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 15/02/2019
 * Time: 9:54
 */
$factory->define(App\Models\Category::class, function (Faker\Generator $faker) {
    return [
        'slug' => str_slug($faker->sentence($nbWords = 2, $variableNbWords = true)),
        'createdDate' => $faker->date('Y-m-d','now'),
        'createdBy' => '1',
    ];
});