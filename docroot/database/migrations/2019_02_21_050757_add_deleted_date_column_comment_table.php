<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDeletedDateColumnCommentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('article_comment', function (Blueprint $table) {
            $table->datetime('deletedDate')->nullable();
        });

        Schema::table('article_reply_comment', function (Blueprint $table) {
            $table->datetime('deletedDate')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('article_comment', function (Blueprint $table) {
            $table->datetime('deletedDate')->nullable();
        });

        Schema::table('article_reply_comment', function (Blueprint $table) {
            $table->datetime('deletedDate')->nullable();
        });
    }
}
