<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyUserIdColumnCommentTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Db::statement('ALTER TABLE article_comment CHANGE COLUMN `userId` `userId` varchar(255) NOT NULL;');
        Db::statement('ALTER TABLE article_reply_comment CHANGE COLUMN `userId` `userId` varchar(255) NOT NULL;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Db::statement('ALTER TABLE article_comment CHANGE COLUMN `userId` `userId` BIGINT(20) NOT NULL;');
        Db::statement('ALTER TABLE article_reply_comment CHANGE COLUMN `userId` `userId` BIGINT(20) NOT NULL;');
    }
}
