<?php
/**
 * Created by PhpStorm.
 * User: maleakhi
 * Date: 3/4/19
 * Time: 4:29 PM
 */

namespace App\Events\Article;

use App\Models\Article;

class ArticlePublished extends Event
{
    public $entrylist;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Article $article)
    {
        $this->article = $article;
    }
}
