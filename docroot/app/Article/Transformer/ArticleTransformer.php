<?php
/**
 * Created by PhpStorm.
 * User: salt
 * Date: 04/02/19
 * Time: 13:20
 */

namespace App\Article\Transformer;

use App\Models\Article;
use App\Models\Bookmarks;
use App\Models\Tags;
use League\Fractal\TransformerAbstract;
use App\Helpers\GetDataUser;
use App\Models\Analytic;

class ArticleTransformer extends TransformerAbstract
{
    protected $locale;

    /**
     * List of resources default to include
     *
     * @var array
     */
    protected $defaultIncludes = [
        'translation', 'comment', 'category','revision'
    ];

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(Article $article)
    {
        return [
            'articleId' => $article->id,
            'author' => $this->getDataAuth0($article->authorId),
            'image' => $article->getImageUrlAttribute(),
            'tags' => $this->getDetailTags($article->tags),
            'createdDate' => $article->createdDate,
            'publishedDate' => $article->publishedDate,
            'highlight' =>  $article->sticky,
            'status' =>  $this->getStatus($article->status),
            'isBookmarked' => $this->checkBookmark($article->id),
            'view' => $this->getView($article->id),
            'share' => $this->getShare($article->id)
        ];
    }
    public function getView($id){
        $view = Analytic::where('vasId',$id)
            ->where('type','view')
            ->where('typeVas','article')
            ->first();
        if(empty($view)){
            return 0;
        }
        return $view->count;
    }
    public function getShare($id){
        $share = Analytic::where('vasId',$id)
            ->where('type','share')
            ->where('typeVas','article')
            ->first();
        if(empty($share)){
            return 0;
        }
        return $share->count;
    }
    public function checkBookmark($id){
        $userId = app('request')->input('userId');
        if(is_null($userId)){
           return 0;
        }
        $bookmarks = Bookmarks::where('articleId',$id)
            ->where('type',1)
            ->where('userId',$userId)
            ->where('isDeleted',0)
            ->first();
        if(empty($bookmarks)){
            return 0;
        }
        return 1;
    }
    public function getDetailTags($tags)
    {
        $data = [];
        if(!$tags){
            return $data;
        }
        $tags = substr($tags, 1);
        $tags = substr($tags, 0, -1);
        $tags = explode(",", $tags);

        for ($i = 0; $i < count($tags); $i++) {
            $dataTags = Tags::find($tags[$i]);
            if($dataTags){
                $value['id'] = $dataTags->id;
                $value['slug'] = $dataTags->slug;
            }else{
                $value = null;
            }
            $data[] = $value;
        }

        return $data;

    }

    public function getDataAuth0($userId)
    {
        $dataUser = new GetDataUser();
        return $dataUser->getMinimalProfile($userId);
    }

    /**
     * Include translation
     *
     * @return \League\Fractal\Resource\Collection
     */
    public function includeTranslation(Article $article)
    {
        $locale = app('request')->input('locale');

        if($locale){
            $translation = $article->translation->filter(function ($translation) use($locale) {
                return $translation->locale == $locale;
            })->first();
            if(is_null($translation)){

            }else{
                return $this->item($translation, new ArticleTranslationTransformer());
            }
        }else{
            $translation = $article->translation;

            return $this->collection($translation, new ArticleTranslationTransformer());
        }
    }

    /**
     * Include Comment
     *
     * @return \League\Fractal\Resource\Collection
     */
    public function includeComment(Article $article)
    {
        $comment = $article->comment;

        return $this->collection($comment, new CommentTransformer());
    }

    /**
     * Include Category
     *
     * @return \League\Fractal\Resource\Item
     */
    public function includeCategory(Article $article)
    {
        $category = $article->category;

        return $this->item($category, new CategoryTransformer());
    }
    public function includeRevision(Article $article)
    {
        if($article->status = 3 || $article->status == 2){
            $notes = $article->notes;
            return $this->collection($notes, new ArticleNotesTransformer());
        }

    }
    public function getStatus($status){
        switch ($status) {
            case 1:
                return 'Published';
                break;
            case 0:
                return 'Draft';
                break;
            case 3:
                return 'Revision';
                break;
            case 2:
                return 'Moderation';
                break;
            case 4:
                return 'Rejected';
                break;
            default:
                return 'Draft';
        }
    }
}