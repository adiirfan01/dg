<?php
/**
 * Created by PhpStorm.
 * User: salt
 * Date: 04/02/19
 * Time: 13:20
 */

namespace App\Article\Transformer;

use App\Helpers\GetDataUser;
use App\Models\Bookmarks;
use App\Models\Video;
use App\Models\Tags;
use League\Fractal\TransformerAbstract;

class VideoTransformer extends TransformerAbstract
{
    protected $locale;
    /**
     * List of resources default to include
     *
     * @var array
     */
    protected $defaultIncludes = [
        'detail',
        'revision',
        'category',
        'comment',
    ];

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(Video $video)
    {
        return [
            'videoId' =>  $video->id,
            'categoryId' =>  $video->categoryId,
            'videoUrl' =>  $this->getVideoId($video->videoUrl),
            'author' => $this->getDataAuth0($video->authorId),
            'status' =>  $this->getStatus($video->status),
            'video_type' =>  $this->getVideoType($video->video_type),
            'tags' =>  $this->getDetailTags($video->tags),
            'createdDate' =>  $video->createdDate,
            'createdBy' =>  $this->getDataAuth0($video->createdBy),
            'publishedDate' =>  $video->publishedDate,
            'highlight' =>  $video->highlight,
            'isBookmarked' => $this->checkBookmark($video->id),
        ];
    }
    public function checkBookmark($id){
        $userId = app('request')->input('userId');
        if(is_null($userId)){
            return 0;
        }
        $bookmarks = Bookmarks::where('articleId',$id)
            ->where('type',2)
            ->where('userId',$userId)
            ->where('isDeleted',0)
            ->first();
        if(empty($bookmarks)){
            return 0;
        }
        return 1;
    }
    public function getVideoId($data){
        $data = str_replace('https://youtu.be/','',$data);
        return $data;
    }
    public function getDetailTags($tags)
    {
        $data = [];
        if(!$tags){
            return $data;
        }
        $tags = substr($tags, 1);
        $tags = substr($tags, 0, -1);
        $tags = explode(",", $tags);

        for ($i = 0; $i < count($tags); $i++) {
            $dataTags = Tags::find($tags[$i]);
            if(!empty($dataTags)){
                $value['id'] = $dataTags->id;
                $value['slug'] = $dataTags->slug;
            }else{
                $value = null;
            }
            $data[] = $value;
        }

        return $data;

    }
    public function getVideoType($data){
        if($data == 1){
            return "live";
        }
        else {
            return "video";
        }
    }

    public function getDataAuth0($userId)
    {
        $dataUser = new GetDataUser();
        return $dataUser->getMinimalProfile($userId);
    }

    /**
     * Include translation
     *
     * @return \League\Fractal\Resource\Collection
     */
    public function includeDetail(Video $video)
    {
        $locale = app('request')->input('locale');

        if($locale){
            $translation = $video->detail->filter(function ($translation) use($locale) {
                return $translation->locale == $locale;
            })->first();
            if(is_null($translation)){
                return;
            }else{
                return $this->item($translation, new VideoDetailTransformer());
            }
        }else{
            $translation = $video->translation;

            return $this->collection($translation, new VideoDetailTransformer());
        }
    }
    public function includeRevision(Video $video)
    {
        if($video->status == 3 || $video->status == 2){
            $detail = $video->revision;

            return $this->collection($detail, new RevisionTransformer());
        }
    }
    public function includeCategory(Video $video)
    {
        $category = $video->category;

        return $this->item($category, new CategoryTransformer());
    }
    public function includeComment(Video $video)
    {
        $comment = $video->comment;

        return $this->collection($comment, new VideoCommentTransformer());
    }
    public function getStatus($status){
        switch ($status) {
            case 1:
                return 'Published';
                break;
            case 0:
                return 'Draft';
                break;
            case 3:
                return 'Revision';
                break;
            case 2:
                return 'Moderation';
                break;
            case 4:
                return 'Rejected';
                break;
            default:
                return 'Draft';
        }
    }
}