<?php

namespace App\Http\Request;

use App\Http\Request\FormRequest;
use Illuminate\Validation\Rule;

class StoreEditCommentRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }


    protected function getValidatorInstance()
    {
        $this->getInputSource()->replace($this->modifyData());
        $validator = parent::getValidatorInstance();

        return $validator;
    }

    protected function modifyData()
    {
        $data = $this->validationData();
        if(array_key_exists('userId', $data)){
            $data['updatedBy'] = $data['userId'];
        }else{
            $data['updatedBy'] = "SYSTEM";
        }
        return $data;
    }
}
