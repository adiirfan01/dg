<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

// Middleware
$router->get('/api/callback', function () use ($router) {
    return $router->app->version();
});
$router->group(['middleware' => 'lang'], function () use ($router) {

    $router->group(['prefix' => 'v1/promotion'], function () use ($router) {

        $router->group(['prefix' => 'articles'], function () use ($router) {
            $router->get('/', 'V1\Promotion\ArticlePromotionController@index');
            $router->get('/id/{slug}', 'V1\Promotion\ArticlePromotionController@getPromotionBySlug');
            $router->get('/tag', 'V1\Promotion\ArticlePromotionController@getPromotionByTag');
        });

    });

    $router->group(['prefix' => 'v1/articles'], function () use ($router) {
        //Article
        $router->group(['middleware' => 'authNonBlock'], function () use ($router) {
            $router->get('/{slug}', 'V1\Article\BaseController@getArticleBySlug');
            $router->get('/{slug}/related', 'V1\Article\BaseController@getRelated');
            $router->get('/', 'V1\Article\ArticleController@index');
        });
        $router->group(['middleware' => 'auth'], function () use ($router) {
            $router->put('/draft', 'V1\Article\ArticleController@saveToDraft');
            $router->post('/{id}/draft', 'V1\Article\ArticleController@editDraft');
            $router->post('/{id}', 'V1\Article\ArticleController@edit');
            $router->delete('/{id}', 'V1\Article\ArticleController@delete');
            $router->post('/{slug}/bookmarks', 'V1\Article\ArticleController@bookmarks');
            $router->post('/{slug}/unbookmarks', 'V1\Article\ArticleController@unbookmarks');
        });

        $router->group(['middleware' => 'auth'], function () use ($router) {
            //UGC
            $router->put('/{id}/revision', 'V1\Article\BaseController@addRevision');
            $router->post('/revision/{id}', 'V1\Article\BaseController@editRevision');
            $router->post('/revision/{id}/solve', 'V1\Article\BaseController@solve');
            $router->post('/{slug}/submit', 'V1\Article\ArticleController@submit');
            $router->post('/{id}/publish', 'V1\Article\ArticleController@publish');
            $router->post('/{id}/rejected', 'V1\Article\ArticleController@rejected');
            $router->post('/{id}/highlight', 'V1\Article\ArticleController@addHighlight');
            $router->post('/{id}/unhighlight', 'V1\Article\ArticleController@removeHighlight');
        });
    });
    $router->group(['prefix' => 'v1/video'], function () use ($router) {
        $router->group(['middleware' => 'authNonBlock'], function () use ($router) {
            $router->get('/', 'V1\Video\VideoController@index');
            $router->get('/{slug}', 'V1\Video\BaseController@getVideoBySlug');
            $router->get('/{slug}/related', 'V1\Video\BaseController@getRelated');
        });


        $router->group(['middleware' => 'auth'], function () use ($router) {
            $router->put('/', 'V1\Video\VideoController@save');
            $router->put('/draft', 'V1\Video\VideoController@saveToDraft');
            $router->post('/{id}', 'V1\Video\VideoController@update');
            $router->post('/{id}/draft', 'V1\Video\VideoController@updateDraft');
            $router->put('/{id}/revision', 'V1\Video\BaseController@addRevision');
            $router->post('/revision/{id}', 'V1\Video\BaseController@editRevision');
            $router->post('/{slug}/submit', 'V1\Video\VideoController@submit');
            $router->post('/revision/{id}/solve', 'V1\Video\BaseController@solve');
            $router->post('/{id}/publish', 'V1\Video\VideoController@publish');
            $router->post('/{id}/highlight', 'V1\Video\VideoController@addHighlight');
            $router->post('/{id}/unhighlight', 'V1\Video\VideoController@removeHighlight');
            $router->post('/{id}/rejected', 'V1\Video\VideoController@rejected');
            $router->delete('/{id}', 'V1\Video\VideoController@delete');
            $router->post('/{slug}/bookmarks', 'V1\Video\VideoController@bookmarks');
            $router->post('/{slug}/unbookmarks', 'V1\Video\VideoController@unbookmarks');
        });
    });


    $router->group(['prefix' => 'v1/my-article'], function () use ($router) {
        $router->group(['middleware' => 'auth'], function () use ($router) {
            $router->get('/', 'V1\Article\BaseController@getMyArticle');
        });
    });
    $router->group(['prefix' => 'v1/my-bookmarks'], function () use ($router) {
        $router->group(['middleware' => 'auth'], function () use ($router) {
            $router->get('/', 'V1\All\BaseController@getMyBookmarks');
        });
    });
    $router->group(['prefix' => 'v1/my-video'], function () use ($router) {
        $router->group(['middleware' => 'auth'], function () use ($router) {
            $router->get('/', 'V1\Video\BaseController@getMyVideo');
        });
    });
    $router->group(['prefix' => 'v1/category'], function () use ($router) {

        $router->get('/', 'V1\Category\CategoryController@index');
        $router->get('/{slug}', 'V1\Category\CategoryController@getCategoryBySlug');

        $router->group(['middleware' => 'auth'], function () use ($router) {
            $router->put('/', 'V1\Category\CategoryController@save');
            $router->post('/{slug}', 'V1\Category\CategoryController@edit');
            $router->delete('/{slug}', 'V1\Category\CategoryController@delete');
        });

    });
    $router->group(['prefix' => 'v1/search'], function () use ($router) {
        $router->group(['middleware' => 'authNonBlock'], function () use ($router) {
            $router->get('/', 'V1\Search\SearchController@index');
        });
    });
    $router->group(['prefix' => 'v1/all'], function () use ($router) {
        $router->group(['middleware' => 'authNonBlock'], function () use ($router) {
            $router->get('/', 'V1\All\AllController@index');
            $router->get('/latest', 'V1\All\AllController@latest');
            $router->get('/popular', 'V1\Analytic\BaseController@getDataWeekly');
        });
    });
    $router->group(['prefix' => 'v1/vas-analytic'], function () use ($router) {
        $router->put('/store', 'V1\Analytic\AnalyticController@store');
        $router->group(['middleware' => 'authNonBlock'], function () use ($router) {
            $router->get('/popularsection', 'V1\Analytic\BaseController@Popular');
        });
    });
});

$router->group(['prefix' => 'v1/tags'], function () use ($router) {

    $router->get('/', 'V1\Tags\TagsController@index');
    $router->get('/{id}', 'V1\Tags\TagsController@getTagsById');

    $router->group(['middleware' => 'auth'], function () use ($router) {
        $router->put('/', 'V1\Tags\TagsController@save');
        $router->post('/{slug}', 'V1\Tags\TagsController@edit');
        $router->delete('/{slug}', 'V1\Tags\TagsController@delete');
    });
});

$router->group(['prefix' => 'v1/comment'], function () use ($router) {
    $router->group(['middleware' => 'authNonBlock'], function () use ($router) {
        $router->get('/{articleId}', 'V1\Comment\CommentController@index');
    });
    $router->group(['middleware' => 'auth'], function () use ($router) {
        $router->put('/{articleId}', 'V1\Comment\CommentController@save');
        $router->post('/{articleId}/{id}', 'V1\Comment\CommentController@edit');
        $router->delete('/{articleId}/{id}', 'V1\Comment\CommentController@delete');
        $router->post('/{articleId}/{id}/like', 'V1\Comment\CommentController@like');
        $router->post('/{articleId}/{id}/dislike', 'V1\Comment\CommentController@dislike');
        $router->post('/{articleId}/{id}/unlike', 'V1\Comment\CommentController@unlike');
    });
});
$router->group(['prefix' => 'v1/videocomment'], function () use ($router) {
    $router->group(['middleware' => 'authNonBlock'], function () use ($router) {
        $router->get('/{videoId}', 'V1\VideoComment\VideoCommentController@index');
    });
    $router->group(['middleware' => 'auth'], function () use ($router) {
        $router->put('/{videoId}', 'V1\VideoComment\VideoCommentController@save');
        $router->post('/{videoId}/{id}', 'V1\VideoComment\VideoCommentController@edit');
        $router->delete('/{videoId}/{id}', 'V1\VideoComment\VideoCommentController@delete');
        $router->post('/{videoId}/{id}/like', 'V1\VideoComment\VideoCommentController@like');
        $router->post('/{videoId}/{id}/dislike', 'V1\VideoComment\VideoCommentController@dislike');
        $router->post('/{videoId}/{id}/unlike', 'V1\VideoComment\VideoCommentController@unlike');
    });
});

$router->group(['prefix' => 'v1/my-comment'], function () use ($router) {
    $router->group(['middleware' => 'auth'], function () use ($router) {
        $router->get('/', 'V1\Comment\BaseController@getMyComment');
    });
});
$router->group(['prefix' => 'v1/my-video-comment'], function () use ($router) {
    $router->group(['middleware' => 'auth'], function () use ($router) {
        $router->get('/', 'V1\VideoComment\BaseController@getMyComment');
    });
});

$router->group(['prefix' => 'v1/editor-review'], function () use ($router) {
    $router->group(['middleware' => 'auth'], function () use ($router) {
        $router->put('/{articleId}', 'V1\Review\ReviewController@save');
        $router->post('/{articleId}/{id}', 'V1\Review\ReviewController@edit');
        $router->delete('/{articleId}/{id}', 'V1\Review\ReviewController@delete');
    });
    $router->get('/{articleId}/{id}', 'V1\Review\ReviewController@single');
    $router->get('/', 'V1\Review\ReviewController@index');
});

$router->group(['prefix' => 'v1/my-review'], function () use ($router) {
    $router->group(['middleware' => 'auth'], function () use ($router) {
        $router->get('/', 'V1\Review\ReviewController@getMyReview');
    });
});

$router->group(['prefix' => 'v1/commentreply'], function () use ($router) {
    $router->group(['middleware' => 'auth'], function () use ($router) {
        $router->put('/{commentId}', 'V1\ReplyComment\ReplyCommentController@save');
        $router->post('/{commentId}/{id}', 'V1\ReplyComment\ReplyCommentController@edit');
        $router->delete('/{commentId}/{id}', 'V1\ReplyComment\ReplyCommentController@delete');
    });
});
$router->group(['prefix' => 'v1/videocommentreply'], function () use ($router) {
    $router->group(['middleware' => 'auth'], function () use ($router) {
        $router->put('/{commentId}', 'V1\VideoReplyComment\VideoReplyCommentController@save');
        $router->post('/{commentId}/{id}', 'V1\VideoReplyComment\VideoReplyCommentController@edit');
        $router->delete('/{commentId}/{id}', 'V1\VideoReplyComment\VideoReplyCommentController@delete');
    });
});

$router->group(['prefix' => 'v1/my-reply-comment'], function () use ($router) {
    $router->group(['middleware' => 'auth'], function () use ($router) {
        $router->get('/', 'V1\ReplyComment\BaseController@getMyReplyComment');
    });
});
$router->group(['prefix' => 'v1/my-video-reply-comment'], function () use ($router) {
    $router->group(['middleware' => 'auth'], function () use ($router) {
        $router->get('/', 'V1\VideoReplyComment\BaseController@getMyReplyComment');
    });
});
$router->group(['prefix' => 'v1/media'], function () use ($router) {
    $router->group(['middleware' => 'auth'], function () use ($router) {
        $router->post('/', 'V1\Media\MediaController@store');
        $router->post('/{uuid}', 'V1\Media\MediaController@update');
        $router->delete('/{uuid}', 'V1\Media\MediaController@remove');
        $router->get('/', 'V1\Media\MediaController@index');
    });
});

