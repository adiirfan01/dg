<?php

namespace App\Article\Serializer;

use League\Fractal\Serializer\ArraySerializer as FractalSerializer;

class ArraySerializer extends FractalSerializer
{
    private $message;

    private $status_code;

    public function __construct($message = null, $status_code = null, $onlyData = true)
    {
        $this->message = $message ?? 'success';
        $this->status_code = $status_code ?? 0;
    }

    /**
     * Serialize an item.
     *
     * @param string $resourceKey
     * @param array  $data
     *
     * @return array
     */
    public function item($resourceKey, array $data)
    {
        return [
            'status' => [
                'code'    => (integer) $this->status_code,
                'message' => (string) $this->message
            ],
            'data' => $data
        ];
    }

}
