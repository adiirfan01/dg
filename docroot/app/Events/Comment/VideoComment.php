<?php

namespace App\Events\Comment;

use Event;
use App\Models\VideoComment as Comment;

class VideoComment extends Event
{
    public $video;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Comment $videoComment)
    {
        $this->videoComment = $videoComment;
    }
}
