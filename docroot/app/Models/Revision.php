<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Revision extends Model
{
    protected $table = 'video_revision';
    protected $fillable= [
        'videoId',
        'content',
        'comment',
        'status',
        'userId',
        'createdBy',
    ];
    public function video(){
        return $this->belongsTo(Video::class,'videoId');
    }
}
