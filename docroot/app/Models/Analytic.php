<?php

namespace App\Models;

use App\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Model;

class Analytic extends Model
{
    use UuidTrait;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'vasAnalytic';

    const CREATED_AT = 'createdDate';
    const UPDATED_AT = 'updatedDate';

    /**
     * @var array
     */
    protected $fillable = [
        'uuid',
        'vasId',
        'categoryId',
        'type',
        'count',
        'locale',
        'typeVas',
        'createdDate',
        'updatedDate'
    ];

}
