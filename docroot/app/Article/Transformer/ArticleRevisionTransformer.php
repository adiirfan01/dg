<?php
/**
 * Created by PhpStorm.
 * User: salt
 * Date: 04/02/19
 * Time: 13:45
 */

namespace App\Article\Transformer;

use App\Models\ArticleRevision;
use League\Fractal\TransformerAbstract;
use App\Helpers\GetDataUser;

class ArticleRevisionTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = ['article'];

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(ArticleRevision $revision)
    {
        return [
            'uuid' => $revision->uuid,
            'articleId' => $revision->articleId,
            'version' => $revision->version,
            'locale' => $revision->locale,
            'metadata' => $revision->metadata,
            'title' => $revision->title,
            'createdDate' => $revision->createdDate,
            'createdBy' => $this->getDataAuth0($revision->createdBy)
        ];
    }

    /**
     * Include Article
     *
     * @return \League\Fractal\Resource\Item
     */
    public function includeArticle(ArticleRevision $revision)
    {
        $article = $revision->article;

        return $this->item($article, new ArticleTransformer());
    }

    public function getDataAuth0($userId)
    {
        $dataUser = new GetDataUser();
        return $dataUser->getMinimalProfile($userId);
    }


}