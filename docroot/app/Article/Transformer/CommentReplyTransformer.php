<?php
/**
 * Created by PhpStorm.
 * User: salt
 * Date: 04/02/19
 * Time: 13:47
 */

namespace App\Article\Transformer;

use App\Models\CommentReply;
use League\Fractal\TransformerAbstract;
use App\Helpers\GetDataUser;
use Carbon\Carbon;


class CommentReplyTransformer extends TransformerAbstract
{
    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(CommentReply $commentReply)
    {
        return [
            'commentReplyId' => $commentReply->commentId,
            'userId' => $this->getDataAuth0($commentReply->userId),
            'comment' => $commentReply->comment,
            'dateNow' => Carbon::now(),
            'createdDate' => $commentReply->createdDate,
            'updatedDate' => $commentReply->updatedDate
        ];
    }

    public function getDataAuth0($userId)
    {
        $dataUser = new GetDataUser();
        return $dataUser->getMinimalProfile($userId);
    }
}