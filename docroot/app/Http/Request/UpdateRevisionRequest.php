<?php

namespace App\Http\Request;

class UpdateRevisionRequest extends FormRequest
{
    protected function validationData()
    {
        $data = $this->merge($this->all());
        return $data->request->all();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'userId' => 'required',
        ];
    }


    protected function getValidatorInstance()
    {
        $this->getInputSource()->replace($this->modifyData());
        $validator = parent::getValidatorInstance();

        return $validator;
    }

    protected function modifyData()
    {
        $data = $this->validationData();

        if(array_key_exists('userId', $data)) {
            $data['createdBy'] = $data['userId'];
        }
        return $data;
    }
}
