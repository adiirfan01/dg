<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeVasInAnalyticTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vasAnalytic', function (Blueprint $table) {
            //
            $table->enum('typeVas', ['article', 'video'])->default('article');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vasAnalytic', function (Blueprint $table) {
            //
            $table->dropColumn('typeVas');
        });
    }
}
