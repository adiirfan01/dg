<?php
/**
 * Created by PhpStorm.
 * User: salt
 * Date: 14/01/19
 * Time: 14:11
 */

return [
    'custom' => [
        'userId' => [
            'required' => 'auth0id must include',
        ],
        'excerpt' => [
            'required' => 'excerpt must include',
        ],
        'content' => [
            'required' => 'content must include',
        ],
        'categoryId' => [
            'required' => 'category must include',
        ],
        'tags' => [
            'required' => 'tags must include',
        ],
        'slug' => [
            'required' => 'slug must include',
        ],
        'title' => [
            'required' => 'title must include',
        ],
    ],
];