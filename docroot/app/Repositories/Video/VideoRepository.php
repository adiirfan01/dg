<?php
/**
 * Created by PhpStorm.
 * User: salt
 * Date: 04/02/19
 * Time: 12:14
 */

namespace App\Repositories\Video;

use App\Models\Video;
use App\Models\Revision;
use App\Models\VideoDetail;
use App\Models\Bookmarks;
use App\Repositories\Video\DetailRepository as Detail;
use App\Repositories\RepositoryInterface;
use Illuminate\Database\Eloquent\Model;
use \Cviebrock\EloquentSluggable\Services\SlugService;
use phpDocumentor\Reflection\Types\Array_;

class VideoRepository implements RepositoryInterface
{
    public function all(array $columns = ['*'])
    {
        $status = app('request')->get('status');
        $user = app('request')->get('user');
        $highlight = app('request')->get('highlight');
        $limit = app('request')->get('limit');
        $page = app('request')->get('page');
        $type = app('request')->get('type');
        $userpref = [];
        $locale = app('request')->input('locale');

        $videoDetail = VideoDetail::where('locale', $locale)->get();
        $videoid = array_map(function ($object) { return $object->videoId; }, json_decode($videoDetail));

        $videoBuilder = Video::filterStatus($status)
            ->filterHighlight($highlight)
            ->User($user)
            ->filterType($type)
            ->whereIn('id', $videoid);
        if(empty($user)){
            $videoBuilder = $videoBuilder->UserPref($userpref);
        }
        if (is_null($page)) {
            return $videoBuilder->get();
        }

        return $videoBuilder->paginate(
            $limit ?? 5,
            ['*'],
            'page',
            $page ?? 1
        );
    }

    public function count() : int
    {
        return Video::count();
    }

    public function paginate(int $perPage = 15, $columns = ['*'])
    {
        return null;
    }

    public function create(array $data): Model
    {
        $video = Video::create($data);
        $videoData['videoId'] = $video->id;
        $data = array_merge($data, $videoData);
        $slug['slug'] = SlugService::createSlug(VideoDetail::class, 'slug', $data['title']);
        $data = array_merge($data, $slug);
        $detail = new VideoDetail();
        $detail->create($data);
        return $video;
    }

    public function update(array $data, int $id)
    {
        $video = $this->find($id);
        $video->update($data);

        $videoData['videoId'] = $id;
        $data = array_merge($data, $videoData);

        $getDataDetail = VideoDetail::where(["videoId" => $video->id, 'locale' => $data['locale']])->first();
        if(!empty($data['title'])){
            $slug['slug'] = SlugService::createSlug(VideoDetail::class, 'slug', $data['title']);
            $data = array_merge($data, $slug);
        }
        $detail = new Detail();

        if($getDataDetail){
            $detail->update($data, $getDataDetail->id);
        }else{
           $detail->create($data);
        }
        return $video->refresh();
    }

    public function updateBy(string $field, string $value, array $data)
    {
        $video = $this->findBy($field, $value);
        $video->update($data);

        $videoData['videoId'] = $video->id;
        $data = array_merge($data, $videoData);

        $getDataTranslation = VideoDetail::where(["videoId" => $video->id, 'locale' => $data['locale']])->first();
        $translation = new Translation();

        if($getDataTranslation){
            $translation = $translation->update($data, $getDataTranslation->id);
        }else{
            $translation = $translation->create($data);
        }

        $articleSlug['slug'] = $translation->slug;
        $data = array_merge($data, $articleSlug);

        $articleMetadata['metadata'] = json_encode($data);
        $data = array_merge($data, $articleMetadata);

        $getDataVersion = ArticleRevision::where("articleId", $article->id)->first();

        if($getDataVersion){
            $articleVersion['version'] = $getDataVersion->version + 1;
        }else{
            $articleVersion['version'] = 0;
        }

        $data = array_merge($data, $articleVersion);

        $revision = new Revision();
        $revision->create($data);

        return $article->refresh();
    }

    public function delete(int $id)
    {
        $video = $this->find($id);
        $video->delete();
        $video_detail = VideoDetail::Query()->where('videoId',$id)->delete();
    }

    public function find(int $id, $columns = array('*'))
    {
        try {
            $video = Video::findOrFail($id);
        }
        catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            throw new \App\Exceptions\ModelNotFoundException;
        }

        return $video;
    }

    public function findBy(string $field, string $value, $columns = ['*'])
    {
        $status = app('request')->get('status');
        try {
            $video = Video::filterStatus($status)->where($field, $value)->get();
            if(empty($video)){
                throw new \App\Exceptions\ModelNotFoundException;
            }
        }
        catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            throw new \App\Exceptions\ModelNotFoundException;
        }

        return $video;
    }

    public function related($locale, $slug)
    {
        $videodetail = VideoDetail::where(['locale' => $locale, 'slug' => $slug])->first();
        $id = $videodetail->videoId;
        $video = $this->find($id);
        $categoryId = $video->categoryId;
        try {
            $video = Video::Related($categoryId,$id)->get();
        }
        catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            throw new \App\Exceptions\ModelNotFoundException;
        }

        return $video;
    }
    public function getBySlug($locale, $slug)
    {
        try {
            $detail = VideoDetail::where(['locale' => $locale, 'slug' => $slug])->first();
        }
        catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            throw new \App\Exceptions\ModelNotFoundException;
        }
        $videoId = $detail->videoId;

        $video = $this->find($videoId);

        return $video;
    }
    public function addRevision(array $data,$id){
        $data['videoId'] = $id;
        $notes = Revision::create($data);
        if($notes){
            $video = $this->find($id);
            $video->update([
                'status' => 3
            ]);
        }
        return $video->refresh();
    }
    public function editRevision(array $data,$id){
        $notes = Revision::find($id);
        $notes->update([
            'content' => $data['content'],
            'comment' => $data['comment']
        ]);
        $video = $notes->refresh();
        $video = $this->find($video->videoId);
        return $video;
    }
    public function solveRevision($id){
        $solve = app('request')->get('solve');
        $notes = Revision::find($id);
        if($solve == 1){
            $notes->update([
                'status' => 1,
            ]);
            $video = $notes->refresh();
            $video = $this->find($video->videoId);
        }elseif($solve == 0){
            $notes->update([
                'status' => 0,
            ]);
            $video = $notes->refresh();
            $video = $this->find($video->videoId);
            $video->update([
                'status' => 3
            ]);
        }
        return $video->refresh();
    }
    public function bookmarks($data, $slug,$type,$deleted){
        $video = $this->getBySlug($data->get('locale'),$slug);
        if(empty($video)){
            return null;
        }
        try {
            $bookmarks = Bookmarks::where('articleId',$video->id)
                ->where('type',$type)
                ->where('userId',$data['userId'])
                ->firstOrFail();
        }
        catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            Bookmarks::create([
                'userId' => $data['userId'],
                'articleId' => $video->id,
                'type' => $type,
                'isDeleted' => 0
            ]);
            return $video;
        }
        Bookmarks::where('id',$bookmarks->id)->update([
            'isDeleted' => $deleted,
        ]);
        return $video;
    }
    public function publish($data,$id){
        $video = $this->find($id);
        $video->update($data);

        return $video->refresh();
    }
}