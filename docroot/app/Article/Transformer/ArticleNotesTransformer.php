<?php
/**
 * Created by PhpStorm.
 * User: salt
 * Date: 04/02/19
 * Time: 13:45
 */

namespace App\Article\Transformer;

use App\Models\ArticleNotes;
use League\Fractal\TransformerAbstract;
use App\Helpers\GetDataUser;

class ArticleNotesTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = ['article'];

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(ArticleNotes $notes)
    {
        return [
            'id' => $notes->id,
            'articleId' => $notes->articleId,
            'userId' => $notes->userId,
            'notes' => $notes->notes,
            'content' => $notes->content,
            'createdDate' => $notes->createdDate,
            'createdBy' => $this->getDataAuth0($notes->createdBy),
            'status'=> $notes->status,
        ];
    }

    /**
     * Include Article
     *
     * @return \League\Fractal\Resource\Item
     */
    public function includeArticle(ArticleNotes $notes)
    {
        $article = $notes->article;

        return $this->item($article, new ArticleTransformer());
    }

    public function getDataAuth0($userId)
    {
        $dataUser = new GetDataUser();
        return $dataUser->getMinimalProfile($userId);
    }
}