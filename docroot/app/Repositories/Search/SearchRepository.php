<?php
/**
 * Created by PhpStorm.
 * User: salt
 * Date: 04/02/19
 * Time: 12:14
 */

namespace App\Repositories\Search;

use App\Models\Video;
use App\Models\VideoDetail;
use App\Models\Article;
use App\Models\ArticleTranslation;
use App\Repositories\RepositoryInterface;
use Illuminate\Database\Eloquent\Model;
use \Cviebrock\EloquentSluggable\Services\SlugService;
use phpDocumentor\Reflection\Types\Array_;

class SearchRepository implements RepositoryInterface
{
    public function all(array $columns = ['*'])
    {
        return null;
    }

    public function count() : int
    {
        return null;
    }

    public function paginate(int $perPage = 15, $columns = ['*'])
    {
        return null;
    }

    public function create(array $data): Model
    {
        return null;
    }

    public function update(array $data, int $id)
    {
        return null;
    }

    public function updateBy(string $field, string $value, array $data)
    {
       return null;
    }

    public function delete(int $id)
    {
        return null;
    }

    public function find(int $id, $columns = array('*'))
    {
        return null;
    }

    public function findBy(string $field, string $value, $columns = ['*'])
    {
       return null;
    }
    public function Search()
    {
        $status = 'published';
        $search = app('request')->get('search');
        $limit = app('request')->get('limit');
        if(empty($limit)){
            $limit = 11;
        }
        $page = app('request')->get('page');
        $limitarticle = ceil($limit / 2);
        $limitvideo  = $limit - $limitarticle;
        try {
            $video = VideoDetail::Where('title', 'LIKE', '%'.$search.'%')
                ->get();
            $article = ArticleTranslation::Where('title', 'LIKE', '%'.$search.'%')
                ->get();
        }
        catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            throw new \App\Exceptions\ModelNotFoundException;
        }

        $getvideo = Video::query();
        if($video){
            $videoid = array_map(function ($object) { return $object->videoId; }, json_decode($video));
            $getvideo->filterStatus($status)->whereIn('id', $videoid);
            if (is_null($page)) {
                $data['video'] = $getvideo->get();
            }else{
                $data['video'] = $getvideo->paginate(
                    $limitarticle ?? 8,
                    ['*'],
                    'page',
                    $page ?? 1
                );
            }
        }
        $getarticle = Article::query();
        if($article) {
            $articleid = array_map(function ($object) { return $object->articleId; }, json_decode($article));
            $getarticle->filterStatus($status)->whereIn('id', $articleid);
            if (is_null($page)) {
                $data['article'] = $getarticle->get();
            }else{
                $data['article'] = $getarticle->paginate(
                    $limitvideo ?? 8,
                    ['*'],
                    'page',
                    $page ?? 1
                );
            }
        }
        return $data;
    }
}