<?php
/**
 * Created by PhpStorm.
 * User: salt
 * Date: 31/01/19
 * Time: 16:31
 */
namespace App\Providers;

use App\Http\Controllers\V1\Media\{MediaController, BaseController as BaseMedia};
use Illuminate\Support\ServiceProvider;
use App\Repositories\RepositoryInterface;
use App\Repositories\Media\{
    MediaRepository
};
class MediaServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {


    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->when(MediaController::class)
            ->needs(RepositoryInterface::class)
            ->give(function() {
                return new MediaRepository();
            });
        $this->app->when(BaseMedia::class)
            ->needs(RepositoryInterface::class)
            ->give(function() {
                return new MediaRepository();
            });
    }
}
