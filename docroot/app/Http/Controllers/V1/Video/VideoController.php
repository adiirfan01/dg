<?php
/**
 * Created by PhpStorm.
 * User: salt
 * Date: 04/02/19
 * Time: 13:49
 */

namespace App\Http\Controllers\V1\Video;

use App\Http\Controllers\V1\Video;
use App\Http\Request\StoreDraftRequest;
use App\Http\Request\StorePublishRequest;
use App\Http\Request\StoreRevisionRequest;
use App\Http\Request\StoreVideoRequest;
use App\Http\Request\StoreUpdateVideoRequest;
use Illuminate\Http\Request;

class VideoController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $video = $this->repo->all();
        return $this->getSuccess('success', $video);
    }
    public function single($id,Request $request)
    {
        $video = $this->repo->find($id);

        return $this->getSuccess('success', $video);
    }
    public function related(int $id,Request $request)
    {
        $video = $this->repo->related($id);
        return $this->getSuccess('success', $video);
    }
    public function highlight(Request $request)
    {
        $video = $this->repo->findBy('highlight',1);
        return $this->getSuccess('success', $video);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save(StoreVideoRequest $request)
    {
        $request->merge([
            'status' => 0,
        ]);
        $video = $this->repo->create($request->all());
        return $this->getSuccess('success', $video);
    }
    public function saveToDraft(StoreDraftRequest $request)
    {
        $request->merge([
            'status' => 0,
        ]);
        $video = $this->repo->create($request->all());
        return $this->getSuccess('success', $video);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(int $id, StoreUpdateVideoRequest $request)
    {
        $request->merge([
            'status' => 0,
        ]);
        $video = $this->repo->update($request->all(), $id);

        return $this->getSuccess('success', $video);
    }
    public function updateDraft(int $id, StoreDraftRequest $request)
    {
        $request->merge([
            'status' => 0,
        ]);
        $video = $this->repo->update($request->all(), $id);
        return $this->getSuccess('success', $video);
    }
    public function publish(int $id,StorePublishRequest $request)
    {
        $video = $this->repo->find($id);
        $request->merge([
            'publishedDate' => date('y-m-d h:i:s'),
            'status' => 1,
        ]);

        $video = $this->repo->publish($request->all(), $id);
        return $this->getSuccess('success', $video);
    }
    public function addHighlight(int $id,StorePublishRequest $request)
    {
        $video = $this->repo->find($id);
        $request->merge([
            'highlight' => 1,
        ]);

        $video = $this->repo->publish($request->all(), $id);
        return $this->getSuccess('success', $video);
    }
    public function removeHighlight(int $id,StorePublishRequest $request)
    {
        $video = $this->repo->find($id);
        $request->merge([
            'highlight' => 0,
        ]);

        $video = $this->repo->publish($request->all(), $id);
        return $this->getSuccess('success', $video);
    }
    public function rejected(int $id,StorePublishRequest $request)
    {
        $video = $this->repo->find($id);
        $request->merge([
            'status' => 4,
        ]);

        $video = $this->repo->publish($request->all(), $id);
        return $this->getSuccess('success', $video);
    }
    public function submit(string $slug,StorePublishRequest $request)
    {
        $video = $this->repo->getBySlug($request->get('locale'), $slug);
        if(!isset($video->status)){
            return $this->getError('Video Tidak Ada');
        }
        if(!$video->status == 0){
            return $this->getError('Submit gagal');
        }
        $request->merge([
            'status' => 2,
        ]);

        $video = $this->repo->publish($request->all(), $video->id);
        return $this->getSuccess('success', $video);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $video = $this->repo->delete($id);

        return $this->getSuccess('success', $video);
    }
    public function bookmarks($slug, Request $request)
    {
        $video = $this->repo->bookmarks($request,$slug,2,0);
        return $this->getSuccess('success', $video);
    }
    public function unbookmarks($slug, Request $request)
    {
        $video = $this->repo->bookmarks($request,$slug,2,1);
        return $this->getSuccess('success', $video);
    }
}