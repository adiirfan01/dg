<?php
/**
 * Created by PhpStorm.
 * User: salt
 * Date: 04/02/19
 * Time: 13:45
 */

namespace App\Article\Transformer;

use App\Models\VideoLikeComment;
use League\Fractal\TransformerAbstract;

class VideoLikeCommentTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $defaultIncludes = [];

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(VideoLikeComment $comment)
    {
        return [
            'like' => $this->countLike($comment->commentId),
            'dislike' => $this->countDislike($comment->commentId),
        ];
    }
    public function countLike($id){
        $total = VideoLikeComment::where('commentId',$id)
            ->where('type',1)
            ->where('isDeleted',0)
            ->count();
        return $total;
    }
    public function countDislike($id){
        $total = VideoLikeComment::where('commentId',$id)
            ->where('type',0)
            ->where('isDeleted',0)
            ->count();
        return $total;
    }
}