<?php
/**
 * Created by PhpStorm.
 * User: salt
 * Date: 04/02/19
 * Time: 12:14
 */

namespace App\Repositories\Analytic;

use App\Models\Analytic;
use App\Repositories\RepositoryInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\Article;
use App\Models\Video;

class AnalyticRepository implements RepositoryInterface
{
    public function all(array $columns = ['*'])
    {
        return Analytic::get();
    }
    public function count() : int
    {
        return Analytic::count();
    }

    public function paginate(int $perPage = 15, $columns = ['*'])
    {
        return null;
    }

    public function create(array $data): Model
    {
        $analytic = Analytic::create($data);

        return $analytic;
    }

    public function update(array $data, int $id)
    {
        return null;
    }

    public function updateBy(string $field, string $value, array $data)
    {
        return null;
    }

    public function delete(int $id)
    {
        return null;
    }

    public function find(int $id, $columns = array('*'))
    {
        try {
            $analytic = Analytic::findOrFail($id);
        }
        catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            throw new \App\Exceptions\ModelNotFoundException;
        }

        return $analytic;
    }

    public function findBy(string $field, string $value, $columns = ['*'])
    {
        try {
            $analytic = Analytic::where($field, $value)->get();
        }
        catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            throw new \App\Exceptions\ModelNotFoundException;
        }

        return $analytic;
    }

    public function dateRange(int $weekly)
    {
        $page  = app('request')->get('page');
        $limit  = app('request')->get('limit');
        if($weekly){
            $from = strtotime("-1 week");
        }else{
            $from = strtotime("-1 day");
        }

        $from = date("Y-m-d", $from);
        $to = date('Y-m-d H:i:s');

        try {
            $analytic = Analytic::selectRaw('SUM(count) AS total,vasId,typeVas')->whereBetween('updatedDate', [$from, $to])
                ->groupBy('vasId', 'typeVas');
            if (is_null($page)) {
                return $analytic->get();
            }
            else{
                return $analytic->orderBy('total', 'DESC')->paginate(
                    $limit ?? 4,
                    ['*'],
                    'page',
                    $page ?? 1
                );
            }
        }
        catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            throw new \App\Exceptions\ModelNotFoundException;
        }
        return $analytic;
    }

    public function store(array $data)
    {
        $checkAnalytic = Analytic::where(['typeVas' => array_get($data, 'typeVas'),
            'vasId' => array_get($data, 'vasId'),
            'type' => array_get($data, 'type'),])->first();

        if($checkAnalytic){
            $data = array_merge([
                'count' => $checkAnalytic->count + 1
            ], $data);

            $checkAnalytic->update($data);
            $analytic =  $checkAnalytic->refresh();
        }else{
            $data = array_merge([
                'count' => 1,
            ], $data);
            $analytic = $this->create($data);
        }

        return $analytic;
    }
    public function allVideo(array $videoid)
    {
        $page = null;
        $status = null;
        $videoBuilder = Video::filterStatus($status)
            ->whereIn('id', $videoid);
        if (is_null($page)) {
            return $videoBuilder->get();
        }
        return $videoBuilder->orderBy('id', 'DESC')->paginate(
            $limit ?? 4,
            ['*'],
            'page',
            $page ?? 1
        );
    }
}