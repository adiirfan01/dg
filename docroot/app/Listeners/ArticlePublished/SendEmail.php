<?php

namespace App\Listeners\ArticlePublished;

use App\Events\Article\ArticlePublished;


/**
 * Class: SendEmail
 *
 * Send email to all member after joining the tournament
 */
class SendEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  EntrylistAdded  $event
     * @return void
     */
    public function handle(ArticlePublished $event)
    {
        $authorId = $event->article->authorId ?? null;

        if($authorId){
            $user = $this->getUser($authorId);

            // TODO email user using data from
        }
    }
    public function getUser($authorId){
        //TODO grab userdata from user Service
        return null;
    }
}
