<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;

/**
 * @property int $id
 * @property int $articleId
 * @property int $userId
 * @property string $notes
 * @property \Carbon\Carbon $createdDate
 * @property string $createdBy
 * @property \Carbon\Carbon $updatedDate
 * @property string $updatedBy
 * @property boolean $isDeleted
 */
class ArticleNotes extends Model
{

    use SoftDeletes;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'article_notes';

    const CREATED_AT = 'createdDate';
    const UPDATED_AT = 'updatedDate';
    const DELETED_AT = 'deletedDate';

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'articleId',
        'userId',
        'notes',
        'content',
        'status',
        'isDeleted',
        'createdBy',
        'createdDate',
        'updatedDate',
        'deletedDate',
        'updatedBy'
    ];

    /**
     * Get the Article
     */
    public function article()
    {
        return $this->belongsTo(Article::class, 'articleId');
    }

}
