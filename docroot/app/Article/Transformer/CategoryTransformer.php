<?php
/**
 * Created by PhpStorm.
 * User: salt
 * Date: 04/02/19
 * Time: 13:44
 */

namespace App\Article\Transformer;

use App\Exceptions\CategoryNotFoundException;
use App\Models\Category;
use League\Fractal\TransformerAbstract;
use phpDocumentor\Reflection\Types\Array_;

class CategoryTransformer extends TransformerAbstract
{
    protected $locale;
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $defaultIncludes = ['translation'];

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(Category $category)
    {
        return [
            'categoryId' => $category->id,
            'slug' => $category->slug,
        ];
    }

    /**
     * Include translation
     *
     * @return \League\Fractal\Resource\Collection
     */
    public function includeTranslation(Category $category)
    {
        $locale = app('request')->input('locale');

        if($locale){
            $translation = $category->translation->filter(function ($translation) use($locale) {
                return $translation->locale == $locale;
            })->first();
            if(!empty($translation)){
                return $this->item($translation, new CategoryTranslationTransformer());
            }
        }else{
            $translation = $category->translation;

            return $this->collection($translation, new CategoryTranslationTransformer());
        }

    }
}