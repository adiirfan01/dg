<?php
/**
 * Created by PhpStorm.
 * User: salt
 * Date: 31/01/19
 * Time: 16:31
 */
namespace App\Providers;

use App\Http\Controllers\V1\Video\{VideoController, BaseController as BaseVideo};
use App\Http\Controllers\V1\VideoComment\{VideoCommentController, BaseController as BaseVideoComment};
use App\Http\Controllers\V1\VideoReplyComment\{VideoReplyCommentController, BaseController as BaseVideoReply};
use App\Http\Controllers\V1\Revision\RevisionController;
use Illuminate\Support\ServiceProvider;
use App\Repositories\RepositoryInterface;
use App\Repositories\Video\{
    VideoRepository,
    VideoCommentRepository,
    VideoReplyCommentRepository,
    RevisionRepository
};
class VideoServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {


    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->when(VideoController::class)
            ->needs(RepositoryInterface::class)
            ->give(function() {
                return new VideoRepository();
            });

        $this->app->when(BaseVideo::class)
            ->needs(RepositoryInterface::class)
            ->give(function() {
                return new VideoRepository();
            });

        $this->app->when(RevisionController::class)
            ->needs(RepositoryInterface::class)
            ->give(function() {
                return new RevisionRepository();
            });

        $this->app->when(VideoCommentController::class)
            ->needs(RepositoryInterface::class)
            ->give(function() {
                return new VideoCommentRepository();
            });

        $this->app->when(BaseVideoComment::class)
            ->needs(RepositoryInterface::class)
            ->give(function() {
                return new VideoCommentRepository();
            });

        $this->app->when(VideoReplyCommentController::class)
            ->needs(RepositoryInterface::class)
            ->give(function() {
                return new VideoReplyCommentRepository();
            });

        $this->app->when(BaseVideoReply::class)
            ->needs(RepositoryInterface::class)
            ->give(function() {
                return new VideoReplyCommentRepository();
            });
    }
}
