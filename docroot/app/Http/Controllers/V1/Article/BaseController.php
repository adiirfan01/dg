<?php
/**
 * Created by PhpStorm.
 * User: salt
 * Date: 04/02/19
 * Time: 13:49
 */

namespace App\Http\Controllers\V1\Article;

use App\Http\Controllers\V1\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Request\StoreRevisionArticleRequest;
use App\Models\ArticleNotes;

class BaseController extends Controller
{
    public function getMyArticle(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->getErrorValidation($validator->getMessageBag()->first());
        }

        $article = $this->repo->findBy('authorId', $request->userId);

        return $this->getSuccess('success', $article);
    }

    public function getArticleBySlug($slug, Request $request)
    {
        $article = $this->repo->getBySlug($request->get('locale'), $slug);

        return $this->getSuccess('success', $article);
    }

    public function getRelated($slug, Request $request)
    {
        $article = $this->repo->getRelated($request->get('locale'), $slug);

        return $this->getSuccess('success', $article);
    }
    public function addRevision($id, StoreRevisionArticleRequest $request)
    {
        $article = $this->repo->addRevision($request->all(), $id);

        return $this->getSuccess('success', $article);
    }
    public function editRevision($id, StoreRevisionArticleRequest $request)
    {
        $article = $this->repo->editRevision($request->all(), $id);

        return $this->getSuccess('success', $article);
    }
    public function solve($id)
    {
        $article = $this->repo->solveRevision($id);
        return $this->getSuccess('success', $article);
    }
}