<?php

namespace App\Repositories\Article;

use App\Models\Article;
use App\Models\ArticleTranslation;
use App\Repositories\RepositoryInterface;
use Illuminate\Database\Eloquent\Model;

class ArticlePromotionRepository implements RepositoryInterface
{
    public function all(array $columns = ['*'])
    {
        $status = app('request')->get('status');
        $highlight = app('request')->get('highlight');
        $limit = app('request')->get('limit');
        $page = app('request')->get('page');

        $articleBuilder = Article::filterStatus($status)->filterHighlight($highlight);

        if (is_null($page)) {
            return $articleBuilder->get();
        }

        return $articleBuilder->paginate(
            $limit ?? 8,
            ['*'],
            'page',
            $page ?? 1
        );
    }

    public function paginate(int $perPage = 15, $columns = ['*'])
    {
        return null;
    }

    public function create(array $data): Model
    {
        return null;
    }

    public function update(array $data, int $id)
    {
        return null;
    }

    public function updateBy(string $field, string $value, array $data)
    {
        return null;
    }

    public function delete(int $id)
    {
        return null;
    }

    public function findBy(string $field, string $value, $columns = ['*'])
    {
        try {
            $article = ArticleTranslation::where($field, $value)->get();
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            throw new \App\Exceptions\ModelNotFoundException;
        }

        return $article;
    }

    public function getBySlug($locale, $slug)
    {
        try {
            $article = ArticleTranslation::where(['locale' => $locale, 'slug' => $slug])->first();
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            throw new \App\Exceptions\ModelNotFoundException;
        }
        $articleId = $article->articleId;

        $article = $this->find($articleId);

        return $article;
    }

    public function find(int $id, $columns = array('*'))
    {
        try {
            $article = Article::findOrFail($id);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            throw new \App\Exceptions\ModelNotFoundException;
        }

        return $article;
    }

    public function findByArticleID(int $articleId, string $locale, $columns = ['*'])
    {
        $article = new ArticleTranslation();
        try {
            $article = $article->where('articleId', '=', $articleId)->where('locale', '=', $locale)->get();
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            throw new \App\Exceptions\ModelNotFoundException;
        }
        $articleId = $article->id;

        $article = $this->find($articleId);
        return $article;
    }

    public function findByArticleTag(string $tags, string $locale, $columns = ['*'])
    {
        $tags = explode(',', $tags);

        if (is_null($tags)) {
            return null;
        }

        $limit = app('request')->get('limit');
        $page = app('request')->get('page');
        $status = app('request')->get('status');
        $highlight = app('request')->get('highlight');

        $articleBuilder = Article::filterStatus($status)->filterHighlight($highlight)
            ->when($tags, function ($articleBuilder, $tags) {
                for ($i = 0; $i < count($tags); $i++) {
                    if ($tags[$i] != "") {
                        $i == 0
                            ? $articleBuilder->where('tags', 'like', '%' . $tags[$i] . '%')
                            : $articleBuilder->orWhere('tags', 'like', '%' . $tags[$i] . '%');
                    }
                }
            });

        if (is_null($page)) {
            /*try{
                $articleBuilder->get();
            }catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e){
                throw new \App\Exceptions\ModelNotFoundException;
            }

            return $articleBuilder;*/
            return $articleBuilder->get();
        }

        return $articleBuilder->paginate(
            $limit ?? 8,
            ['*'],
            'page',
            $page ?? 1
        );
    }
}