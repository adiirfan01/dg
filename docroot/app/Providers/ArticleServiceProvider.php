<?php
/**
 * Created by PhpStorm.
 * User: salt
 * Date: 31/01/19
 * Time: 16:31
 */
namespace App\Providers;

use App\Http\Controllers\V1\Article\{ArticleController, BaseController as BaseArticle};
use App\Http\Controllers\V1\Comment\{CommentController, BaseController as BaseComment};
use App\Http\Controllers\V1\ReplyComment\{ReplyCommentController, BaseController as BaseReply};
use App\Http\Controllers\V1\Review\{ReviewController, BaseController as BaseReview};
use App\Http\Controllers\V1\Promotion\{ArticlePromotionController, BaseController as BasePromotion};
use Illuminate\Support\ServiceProvider;
use App\Repositories\RepositoryInterface;
use App\Repositories\Article\{
    ArticleRepository,
    CommentRepository,
    ReplyCommentRepository,
    ReviewRepository,
    ArticlePromotionRepository
};
class ArticleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {


    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->when(ArticleController::class)
            ->needs(RepositoryInterface::class)
            ->give(function() {
                return new ArticleRepository();
            });

        $this->app->when(BaseArticle::class)
            ->needs(RepositoryInterface::class)
            ->give(function() {
                return new ArticleRepository();
            });

        $this->app->when(CommentController::class)
            ->needs(RepositoryInterface::class)
            ->give(function() {
                return new CommentRepository();
            });

        $this->app->when(BaseComment::class)
            ->needs(RepositoryInterface::class)
            ->give(function() {
                return new CommentRepository();
            });

        $this->app->when(ReplyCommentController::class)
            ->needs(RepositoryInterface::class)
            ->give(function() {
                return new ReplyCommentRepository();
            });

        $this->app->when(BaseReply::class)
            ->needs(RepositoryInterface::class)
            ->give(function() {
                return new ReplyCommentRepository();
            });

        $this->app->when(ReviewController::class)
            ->needs(RepositoryInterface::class)
            ->give(function() {
                return new ReviewRepository();
            });

        $this->app->when(BaseReview::class)
            ->needs(RepositoryInterface::class)
            ->give(function() {
                return new ReviewRepository();
            });

        $this->app->when(ArticlePromotionController::class)
            ->needs(RepositoryInterface::class)
            ->give(function() {
                return new ArticlePromotionRepository();
            });

        $this->app->when(BasePromotion::class)
            ->needs(RepositoryInterface::class)
            ->give(function() {
                return new ArticlePromotionRepository();
            });
    }
}