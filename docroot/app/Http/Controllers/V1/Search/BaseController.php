<?php
/**
 * Created by PhpStorm.
 * User: salt
 * Date: 04/02/19
 * Time: 13:50
 */

namespace App\Http\Controllers\V1\Search;

use App\Http\Controllers\V1\Controller;

class BaseController extends Controller
{
    public function getTagsBySlug($slug)
    {
        $category = $this->repo->findBy('slug', $slug)->first();

        return $this->getSuccess('success', $category);
    }
}