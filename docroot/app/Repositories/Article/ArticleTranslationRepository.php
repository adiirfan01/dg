<?php
/**
 * Created by PhpStorm.
 * User: salt
 * Date: 04/02/19
 * Time: 12:14
 */

namespace App\Repositories\Article;

use App\Models\ArticleTranslation;
use App\Repositories\RepositoryInterface;
use Illuminate\Database\Eloquent\Model;

class ArticleTranslationRepository implements RepositoryInterface
{
    public function all(array $columns = ['*'])
    {
        return null;
    }

    public function count() : int
    {
        return ArticleTranslation::count();
    }

    public function paginate(int $perPage = 15, $columns = ['*'])
    {
        return null;
    }

    public function create(array $data): Model
    {
        $article = ArticleTranslation::create($data);

        return $article;
    }

    public function update(array $data, int $id)
    {
        $article = $this->find($id);
        $article->update($data);

        return $article->refresh();
    }

    public function updateBy(string $field, string $value, array $data)
    {
        $article = $this->findBy($field, $value);
        $article->update($data);

        return $article->refresh();
    }

    public function delete(int $id)
    {
        return null;
    }

    public function find(int $id, $columns = array('*'))
    {
        try {
            $article = ArticleTranslation::findOrFail($id);
        }
        catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            throw new \App\Exceptions\ModelNotFoundException;
        }

        return $article;
    }

    public function findBy(string $field, string $value, $columns = ['*'])
    {
        try {
            $article = ArticleTranslation::where($field, $value)->get();
        }
        catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            throw new \App\Exceptions\ModelNotFoundException;
        }

        return $article;
    }
}