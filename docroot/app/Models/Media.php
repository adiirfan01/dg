<?php

namespace App\Models;

use App\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    use UuidTrait;
    protected $table = 'media';
    protected $fillable= [
        'uuid',
        'url',
        'userId',
    ];
}
