<?php

namespace App\Models;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;

/**
 * @property int $id
 * @property int $articleId
 * @property int $userId
 * @property string $comment
 * @property \Carbon\Carbon $createdDate
 * @property string $createdBy
 * @property \Carbon\Carbon $updatedDate
 * @property string $updatedBy
 * @property boolean $isDeleted
 */
class Comment extends Model
{
    use SoftDeletes;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'article_comment';

    const CREATED_AT = 'createdDate';
    const UPDATED_AT = 'updatedDate';
    const DELETED_AT = 'deletedDate';

    /**
     * @var array
     */
    protected $fillable = [
        'articleId',
        'userId',
        'comment',
        'isDeleted',
        'createdDate',
        'updatedDate',
        'createdBy',
        'deletedDate',
        'updatedBy'
    ];

    /**
     * Get the Article
     */
    public function article()
    {
        return $this->belongsTo(Article::class, 'articleId');
    }

    /**
     * Get the Reply Comment
     */
    public function replyComment()
    {
        return $this->hasMany(CommentReply::class, 'commentId');
    }

    /**
     * Scope to filter comment by articleId
     *
     * @param Builder $query
     * @param mixed $status
     * @return Builder $query
     */
    public function scopeFilterArticle($query, $articleId)
    {
        if (is_null($articleId)) return $query;

        return $query->where('articleId', $articleId);
    }

}
