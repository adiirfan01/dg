<?php
/**
 * Created by PhpStorm.
 * User: Salt
 * Date: 22/12/2018
 * Time: 22:07
 */

namespace App\Http\Middleware;

use Closure;
use Auth0\SDK\JWTVerifier;
use Illuminate\Http\JsonResponse;

class Auth0NonBlockMiddleware
{
    /**
     * Run the request filter.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     * @throws \Auth0\SDK\Exception\CoreException
     */
    public function handle($request, Closure $next)
    {
        $responseCode = 401;

        if ($request->hasHeader('Authorization')) {
            $authorization = $request->header('Authorization');
        } elseif ($request->hasHeader('authorization')) {
            $authorization = $request->header('authorization');
        } else {
            $request->request->add(['userId' => null]);
            return $next($request);
        }

        $token = null;
        $authorizationHeader = str_replace('bearer ', '', $authorization);
        $token = str_replace('Bearer ', '', $authorizationHeader);

        if ($authorization == null || $token == null) {
            $request->request->add(['userId' => null]);
            return $next($request);
        }
        if (getenv('APP_ENV') == 'production') {
            $guzzleOptions = [
                'proxy' => getenv("PROXY_URL"),
                'verify' => false
            ];
        } else {
            $guzzleOptions = [
                'verify' => false
            ];
        }

        try {
            $verifier = new JWTVerifier([
                'supported_algs' => [getenv('AUTH0_SUPPORTED_ALGS')],
                'valid_audiences' => [getenv('AUTH0_IDENTIFIER')],
                'authorized_iss' => [getenv('AUTH0_AUTHORIZED_IIS')],
                'guzzle_options' => $guzzleOptions
            ]);

            $decoded = $verifier->verifyAndDecode($token);
        } catch (\Auth0\SDK\Exception\CoreException $e) {
            $request->request->add(['userId' => null]);
            return $next($request);
        };

        $request->request->add(['userId' => $decoded->sub]);

        return $next($request);
    }

}
