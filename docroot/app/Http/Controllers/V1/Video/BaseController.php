<?php
/**
 * Created by PhpStorm.
 * User: salt
 * Date: 04/02/19
 * Time: 13:49
 */

namespace App\Http\Controllers\V1\Video;

use App\Http\Controllers\V1\Controller;
use App\Repositories\Video\DetailRepository;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Request\StoreRevisionRequest;

class BaseController extends Controller
{
    public function getMyVideo(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->getErrorValidation($validator->getMessageBag()->first());
        }

        $video = $this->repo->findBy('authorId', $request->userId);

        return $this->getSuccess('success', $video);
    }
    public function getVideoBySlug($slug, Request $request)
    {
        $video = $this->repo->getBySlug($request->get('locale'), $slug);

        return $this->getSuccess('success', $video);
    }

    public function getRelated($slug, Request $request)
    {
        $video = $this->repo->related($request->get('locale'), $slug);

        return $this->getSuccess('success', $video);
    }
    public function addRevision($id, StoreRevisionRequest $request)
    {
        $video = $this->repo->addRevision($request->all(), $id);

        return $this->getSuccess('success', $video);
    }
    public function editRevision($id, StoreRevisionRequest $request)
    {
        $video = $this->repo->editRevision($request->all(), $id);

        return $this->getSuccess('success', $video);
    }
    public function solve($id)
    {
        $video = $this->repo->solveRevision($id);
        return $this->getSuccess('success', $video);
    }
}